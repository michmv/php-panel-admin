<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AuthModule extends Migration
{
    public $prefixModule = 'auth_';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->prefixModule.'users', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email', 64)->unique();
            $table->string('password', 80);
            $table->string('name')->unique();
            $table->boolean('email_confirmed')->index();
            $table->bigInteger('created_at')->index();
            $table->bigInteger('updated_at')->index();
            $table->integer('role')->index('role_id');
        });

        Schema::create($this->prefixModule.'email_confirm', function(Blueprint $table) {
            $table->bigIncrements('record_id');
            $table->bigInteger('user_id')->index('user_id');
            $table->string('code', 80);
            $table->bigInteger('time_life')->index('limit');
        });

        Schema::create($this->prefixModule.'password_reset', function(Blueprint $table) {
            $table->bigIncrements('record_id');
            $table->bigInteger('user_id')->index('user_id');
            $table->string('code', 80);
            $table->bigInteger('time_life')->index('limit');
        });

        Schema::create($this->prefixModule.'failed_signin', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->index();
            $table->string('agent',256)->index();
            $table->tinyInteger('count');
            $table->bigInteger('created_at')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->prefixModule.'users');
        Schema::dropIfExists($this->prefixModule.'email_confirm');
        Schema::dropIfExists($this->prefixModule.'password_reset');
        Schema::dropIfExists($this->prefixModule.'failed_signin');
    }
}
