<?php

namespace MMV\PA\Auth;

use MMV\Auth\Low\StorageInterface;
use Illuminate\Database\DatabaseManager;

class Storage implements StorageInterface
{
    protected string $table = '';

    protected DatabaseManager $db;

    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    public function setTable(string $name)
    {
        $this->table = $name;
    }

    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function insertRecord($data, string $table=null): int
    {
        return $this->db->table($this->table($table))
            ->insertGetId($data);
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param array $data [key => value]
     * @param string|null $table
     * @return int
     */
    public function updateRecord($conditions, $data, string $table=null): int
    {
        $q = $this->db->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->update($data);
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function deleteRecord($conditions, string $table=null): int
    {
        $q = $this->db->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->delete();
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return int
     */
    public function countRecord($conditions, string $table=null): int
    {
        $q = $this->db->table($this->table($table));
        $this->conditions($q, $conditions);

        return $q->count();
    }

    /**
     * @param array $conditions [[name, =, value]]
     * @param string|null $table
     * @return object[] [ [row from table] ]
     */
    public function findRecord($conditions, string $table=null): ?array
    {
        $q = $this->db->table($this->table($table));
        $this->conditions($q, $conditions);

        $result =  $q->get();
        return $result ? $result->all() : [];
    }

    public function beginTransaction()
    {
        $this->db->connection()->beginTransaction();
    }

    public function commitTransaction()
    {
        $this->db->connection()->commit();
    }

    public function rollbackTransaction()
    {
        $this->db->connection()->rollBack();
    }

    protected function table($name)
    {
        return ($name) ? $name : $this->getTable();
    }

    protected function conditions($q, $conditions)
    {
        // var_dump($q);
        // var_dump($conditions);
        foreach($conditions as $i) {
            $q->where($i[0], $i[1], $i[2]);
        }
    }
}
