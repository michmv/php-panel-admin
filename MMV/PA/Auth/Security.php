<?php

namespace MMV\PA\Auth;

use MMV\Auth\Low\Auth\SecurityInterface;
use Illuminate\Contracts\Hashing\Hasher;

use Illuminate\Support\Str;

class Security implements SecurityInterface
{
    protected Hasher $hasher;

    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    public function hash($value, array $options = []): string
    {
        return $this->hasher->make($value, $options);
    }

    public function check(string $value, string $hashedValue, array $options = []): bool
    {
        return $this->hasher->check($value, $hashedValue, $options);
    }

    public function randomString(int $length): string
    {
        return Str::random($length);
    }
}
