<?php

namespace MMV\PA\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $view = '';

    /**
     * @var array
     */
    public $data = [];

    /**
     * @var string
     */
    public $subject = '';

    /**
     * Create a new message instance.
     *
     * @param string $view
     * @param array $data
     * @return void
     */
    public function __construct($view, $subject, $data)
    {
        $this->view = $view;
        $this->subject = $subject;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view($this->view)
            ->with($this->data);
    }
}
