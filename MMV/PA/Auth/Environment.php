<?php

namespace MMV\PA\Auth;

use MMV\Auth\Low\Auth\EnvironmentInterface;
use Illuminate\Http\Request;

class Environment implements EnvironmentInterface
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getAgentId(): string
    {
        $ips = $this->request->getClientIps();
        return substr(implode('|', $ips), 0, 256);
    }
}
