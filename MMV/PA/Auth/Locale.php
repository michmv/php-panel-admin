<?php

namespace MMV\PA\Auth;

use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;

class Locale
{
    public static function makeMessageBag(array $messages): MessageBag
    {
        return new MessageBag($messages);
    }

    public static function trans(Translator $translator, array $messages)
    {
        foreach($messages as $name => $error) {
            foreach($error as $index => $message) {
                list($message_, $params_) = static::parseMessageValidator($message);
                $error[$index] = $translator->get('panel-admin/auth.validator.'.$message_, $params_);
            }
            $messages[$name] = $error;
        }
        return $messages;
    }

    protected static function parseMessageValidator($str)
    {
        $res = [];
        $array = explode('|', $str);
        for($n=1; $n<count($array); $n++) {
            $res['p'.$n] = $array[$n];
        }
        return [$array[0], $res];
    }
}
