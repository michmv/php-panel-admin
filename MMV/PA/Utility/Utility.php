<?php

namespace MMV\PA\Utility;

class Utility
{
    public static function configurator(object $object, array &$properties)
    {
        foreach($properties as $key => $val) {
            if(property_exists($object, $key)) {
                $object->$key = $val;
            }
            else if(method_exists($object, $method = 'set'.$key)) {
                $object->$method($val);
            }
        }
        return $object;
    }

    public static function urlPlusParams(string $url, array $params)
    {
        $res = [];
        foreach($params as $name => $value) {
            $res[] = $name . '=' . urlencode($value);
        }
        return $url . (($res) ? '?' . implode('&', $res) : '');
    }

    /**
     * List to object list
     *
     * @param array $list
     * @param string $defaultClass
     * @return array
     */
    public static function toObjectList($list, $defaultClass)
    {
        $res = [];
        foreach($list as $item) {
            if(is_string($item)) {
                $class = $defaultClass;
                $t = new $class($item);
            }
            else if(is_array($item)) {
                $class = $item['class'] ?? $defaultClass;
                $t = new $class;
                $t = static::configurator($t, $item);
            }
            else {
                $t = $item;
            }
            $res[] = $t;
        }
        return $res;
    }
}
