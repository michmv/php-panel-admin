<?php

namespace MMV\PA\Utility;

use MMV\PA\Helper;
use Illuminate\View\Factory;

abstract class Widget
{
    protected Helper $helper;

    protected Factory $factory;

    public function __construct(Factory $factory, Helper $helper)
    {
        $this->helper = $helper;
        $this->factory = $factory;
    }

    public function view($view = null, $data = [], $mergeData = [])
    {
        return (string)$this->factory->make($view, $data, $mergeData);
    }

    public function __toString()
    {
        return get_class($this);
    }
}
