<?php

namespace MMV\PA\Utility;

use MMV\PA\Console\CreateUser;
use MMV\PA\Session;
use MMV\PA\Auth;
use MMV\PA\Access;
use MMV\PA\Helper;
use MMV\PA\Auth\Security;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Session::class, function($app){
            return new Session($app);
        });

        $this->app->singleton(Auth::class, function($app){
            return new Auth($app);
        });

        $this->app->singleton(Access::class, function($app){
            return new Access($app);
        });

        $this->app->singleton(Helper::class, function($app){
            return new Helper($app);
        });

        $this->app->singleton(Security::class, function($app){
            return new Security($app->make('hash'));
        });

        $this->commands([
            CreateUser::class,
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $elfinderPath = __DIR__.'/../../../../../studio-42/elfinder';

        $this->publishes([
            __DIR__.'/../../../config'     => config_path('panel-admin'),
            __DIR__.'/../../../views'      => resource_path('views'),
            __DIR__.'/../../../lang'       => resource_path('lang'),
            __DIR__.'/../../../migrations' => database_path('migrations'),
            __DIR__.'/../../../assets'     => public_path('assets'),
            __DIR__.'/../../../uploads'    => public_path('uploads'),

            $elfinderPath.'/css'           => public_path('assets/elfinder/css'),
            $elfinderPath.'/js'            => public_path('assets/elfinder/js'),
            $elfinderPath.'/img'           => public_path('assets/elfinder/img'),
        ]);

        $blade = $this->app->make('blade.compiler');
        /** @var BladeCompiler $blade */
        $blade->directive('do', function($expression) {
            return '<?php '.$expression.'; ?>';
        });
    }
}
