<?php

namespace MMV\PA\Utility;

class Header
{
    protected $config = [];

    protected $title = '';

    protected $keywords = '';

    protected $description = '';

    /**
     * @var array String or object
     */
    protected $resources = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getTitle()
    {
        return ($this->title) ? $this->title : ($this->config['SEO']['title'] ?? '');
    }

    public function getTitles(): array
    {
        $res = [];
        if($this->title) $res[] = $this->title;
        if($global = ($this->config['SEO']['title'] ?? '')) $res[] = $global;
        return $res;
    }

    public function setTitle($str)
    {
        $this->title = $str;
    }

    public function getKeywords()
    {
        return ($this->keywords) ? $this->keywords : ($this->config['SEO']['keywords'] ?? '');
    }

    public function setKeywords($str)
    {
        $this->keywords = $str;
    }

    public function getDescription()
    {
        return ($this->description) ? $this->description : ($this->config['SEO']['description'] ?? '');
    }

    public function setDescription($str)
    {
        $this->description = $str;
    }

    /**
     * Undocumented function
     *
     * @param string $key
     * @param object|string $str
     * @return void
     */
    public function addResource(string $key, $str)
    {
        $this->resources[$key] = $str;
    }

    public function getResources()
    {
        // TODO:
        return array_merge($this->config['resources'], $this->resources);
    }
}
