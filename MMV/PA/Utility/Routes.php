<?php

namespace MMV\PA\Utility;

use Illuminate\Routing\Router;

class Routes
{
    public static function set(Router $router)
    {
        $path = '\MMV\PA\Controllers\\';

        // Auth
        $router->any('signin', $path.'Auth@signIn')->name('pa.signIn');
        $router->any('signup', $path.'Auth@signUp')->name('pa.signUp');
        $router->any('password/forgot', $path.'Auth@forgotPassword')->name('pa.forgotPassword');
        $router->any('confirm/email/{id}/{code}', $path.'Auth@confirmEmail')->name('pa.confirmEmail');
        $router->any('password/reset/{id}/{code}', $path.'Auth@resetPassword')->name('pa.resetPassword');
        $router->any('password/change', $path.'Auth@changePassword')->name('pa.changePassword');
        $router->get('signout', $path.'Auth@signOut')->name('pa.signOut');
        $router->get('signout/all', $path.'Auth@signOutAll')->name('pa.signOutAll');

        // Roles
        $router->get('users/roles', $path.'User@roles')->name('pa.users.roles');
        $router->get('users/role/{id}', $path.'User@showRole')->name('pa.users.roles.show');

        // User
        $router->get('users', $path.'User@list')->name('pa.users.list');
        $router->any('users/create', $path.'User@create')->name('pa.users.create');
        $router->any('users/update/{id}', $path.'User@update')->name('pa.users.update');
        $router->post('users/delete/{id}', $path.'User@delete')->name('pa.users.delete');
    }
}
