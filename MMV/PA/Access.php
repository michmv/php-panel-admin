<?php

namespace MMV\PA;

use Illuminate\Foundation\Application;
use MMV\Auth\Low\Access as BaseAccess;

class Access extends BaseAccess
{
    public function __construct(Application $app)
    {
        $config = $app->make('config')->get('panel-admin.access');

        parent::__construct($config['roles'], $config['resources']);
    }
}
