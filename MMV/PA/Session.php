<?php

namespace MMV\PA;

use MMV\Auth\Low\Session as BaseSession;
use MMV\PA\Session\Security;
use MMV\PA\Auth\Storage;
use MMV\PA\Session\Environment;
use Illuminate\Foundation\Application;

class Session extends BaseSession
{
    public function __construct(Application $app)
    {
        $options = $app->make('config')->get('panel-admin.app.session');
        $security = new Security($app->make('encrypter'));
        $storage = new Storage($app->make('db'));
        $storage->setTable($options['table']);
        $environment = new Environment($app->make('cookie'), $app->make('request'));

        parent::__construct($security, $storage, $environment, $options);
    }
}
