<?php

namespace MMV\PA\Session;

use MMV\Auth\Low\Session\SecurityInterface;
use Throwable;
use Ramsey\Uuid\Uuid;
use Illuminate\Encryption\Encrypter;

class Security implements SecurityInterface
{
    protected Encrypter $encrypter;

    public function __construct(Encrypter $encrypter)
    {
        $this->encrypter = $encrypter;
    }

    public function encrypt(string $str): string
    {
        return $this->encrypter->encryptString($str);
    }

    public function decrypt(string $str): string
    {
        try {
            return $this->encrypter->decryptString($str);
        }
        catch (Throwable $e) {
            return '';
        }
    }

    public function uuid(): string
    {
        return str_replace('-', '', Uuid::uuid4());
    }
}
