<?php

namespace MMV\PA\Session;

use MMV\Auth\Low\Session\EnvironmentInterface;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;

class Environment implements EnvironmentInterface
{
    protected CookieJar $cookieJar;

    protected Request $request;

    public function __construct(CookieJar $cookieJar, Request $request)
    {
        $this->cookieJar = $cookieJar;
        $this->request = $request;
    }

    public function setCookie(string $name, string $value = null, $expire = 0, ?string $path = '/', string $domain = null, bool $secure = null, bool $httpOnly = true)
    {
        $minutes = ceil(($expire - time()) / 60);
        $it = $this->cookieJar->make($name, $value, $minutes, $path, $domain, $secure, $httpOnly);
        $this->cookieJar->queue($it);
    }

    public function getCookie(string $name, string $default='')
    {
        return $this->request->cookie($name, $default);
    }
}
