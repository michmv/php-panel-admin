<?php

namespace MMV\PA\Controllers;

use Illuminate\Http\Request;
use MMV\PA\Auth as AuthModule;
use MMV\PA\Session;
use MMV\PA\Controllers\BaseAbstract;
use MMV\PA\Auth\Locale;
use MMV\PA\Auth\SendMail;
use MMV\Auth\Low\User;
use Illuminate\Mail\Mailer;
use MMV\PA\Middleware\SimpleAntiSpam;

class Auth extends BaseAbstract
{
    protected AuthModule $auth;
    
    protected Session $session;

    protected function init()
    {
        $this->auth = $this->app->make(AuthModule::class);
        $this->session = $this->app->make(Session::class);

        $this->middleware(SimpleAntiSpam::class);
    }

    /**
     * SignIn page
     */
    public function signIn(Request $request)
    {
        // check guest
        if(!$this->auth->isGuest()) return $this->redirectStartPage();

        $session = $this->session;
        $view = 'panel-admin.auth.signin';
        $data = [
            'data' => [],
            'errors' => [],
            'message' => ($session->has('message') ? $session->get('message') : ''),
        ];

        // POST
        if($request->method() == 'POST')
            return $this->postSignIn($view, $data, $request);

        if($session->has('email'))
            $data['data']['email'] = $session->get('email');

        return $this->view($view, $data);
    }

    /**
     * SignUp page
     */
    public function signUp(Request $request, Mailer $maller)
    {
        // check turn on registration
        if(!($t = $this->config('panel-admin.app.auth.registration')))
            return $this->abort(404);

        // check guest
        if(!$this->auth->isGuest()) return $this->redirectStartPage();

        $view = 'panel-admin.auth.signup';

        // POST
        if($request->method() == 'POST')
            return $this->postSignUp($view, $request, $maller);

        return $this->view($view, ['data'=>[], 'errors'=>[]]);
    }

    /**
     * Forgot password form
     */
    public function forgotPassword(Request $request, Mailer $maller)
    {
        // check guest
        if(!$this->auth->isGuest()) return $this->redirectStartPage();

        $data = ['data'=>[], 'errors'=>[]];
        $view = 'panel-admin.auth.forgot_password';

        // POST
        if($request->method() == 'POST')
            return $this->postForgotPassword($view, $request, $maller);

        return $this->view($view, $data);
    }

    /**
     * Confirm email
     */
    public function confirmEmail($id, $code)
    {
        if($user = $this->auth->confirmEmail($id, $code)) {

            $this->session->flash('message', $this->trans('panel-admin/auth.messages.successConfirmEmail'));
            $this->session->flash('email', $user->email);

            return $this->redirect()->route('pa.signIn');
        }
        return $this->abort(404);
    }

    /**
     * Reset new password
     */
    public function resetPassword(Request $request, $id, $code)
    {
        $auth = $this->auth;
        if(!$auth->isGuest())
            return $this->redirect()->route('pa.changePassword');

        if($user = $auth->findUserByRecordResetPassword($id, $code)) {
            return $this->changePasswordFunction($request, $auth, $user, $id);
        }

        return $this->abort(404);
    }

    /**
     * Change password for user
     */
    public function changePassword(Request $request)
    {
        $auth = $this->auth;
        if($auth->isGuest()) return $this->abort(403);

        return $this->changePasswordFunction($request, $auth, $auth->user());
    }

    /**
     * SignOut for user
     */
    public function signout()
    {
        if(!$this->auth->isGuest()) {
            $this->auth->signOut();
        }

        return $this->redirectStartPage();
    }

    /**
     * Close all other sessions for user
     */
    public function signoutAll()
    {
        if(!$this->auth->isGuest()) {
            $this->auth->signOUtAll($this->auth->user());
        }

        return $this->redirectStartPage();
    }

    public static function convertErrorMessage(array $errors, $trans)
    {
        return Locale::makeMessageBag(Locale::trans($trans, $errors));
    }

    protected function changePasswordFunction($request, AuthModule $auth, User $user, $recordId=0)
    {
        $view = 'panel-admin.auth.change_password';
        $data = ['data'=>[], 'errors'=>[]];

        // POST
        if($request->method() == 'POST')
            return $this->postChangePassword($request, $view, $user, $recordId, $auth);
    
        $data['user'] = $user;
        return $this->view($view, $data);
    }

    protected function postChangePassword($request, $view, User $user, $recordId, AuthModule $auth)
    {
        $r = $request->request;
        $newPassword = $r->get('password', '');
        $confirmPassword = $r->get('confirm', '');

        if($auth->changePasswordForUser($user, $newPassword, $confirmPassword, $recordId)) {
            // redirect to login form
            $this->session->flash('message', $this->trans('panel-admin/auth.messages.passwordHasBeenChanged'));
            return $this->redirect()->route('pa.signIn');
        }

        return $this->view($view, [
            'data' => [],
            'errors' => static::convertErrorMessage($auth->getMessages(), $this->trans()),
            'user' => $user,
        ]);
    }

    protected function postForgotPassword($view, $request, $maller)
    {
        $email = $request->request->get('to', '');
        $auth = $this->auth;

        $user = $auth->findUserByEmail($email);
        if($user) {
            $auth->beginTransaction();

            // create record for reset password
            list($recordId, $code) = $auth->createResetPassword($user);

            // send email
            $this->sendEmail(
                'panel-admin.lang.'.$this->app->getLocale().'.auth_reset_password',
                $this->trans('panel-admin/auth.subjectForgotPassword'),
                ['user'=>$user, 'record_id'=>$recordId, 'code'=>$code],
                $maller
            );

            $auth->commitTransaction();

            // redirect with message
            $this->session->flash('message', $this->trans('panel-admin/auth.messages.createResetPassword'));
            return $this->redirect()->route('pa.signIn');
        }

        $data = [
            'data'=>['email'=>$email],
            'errors'=>static::convertErrorMessage($auth->getMessages(), $this->trans())
            ];
        return $this->view($view, $data);
    }

    protected function postSignUp($view, $request, $maller)
    {
        /** @var Request $request */
        $r = $request->request;
        $data = [
            'email'    => $r->get('to', ''),
            'name'     => $r->get('name', ''),
            'password' => $r->get('password', ''),
            'confirm'  => $r->get('confirm', ''),
        ];

        $auth = $this->auth;
        $auth->beginTransaction();

        $user = $auth->signUp($data);
        if($user) {
            // user was registered
            $user->email_confirmed = 0;

            // need send confirm email
            list($recordId, $code) = $auth->createEmailConfirm($user);
            $this->sendEmail(
                'panel-admin.lang.'.$this->app->getLocale().'.auth_email_confirm',
                $this->trans('panel-admin/auth.subjectConfirm'),
                ['user'=>$user, 'record_id'=>$recordId, 'code'=>$code],
                $maller
            );

            $auth->commitTransaction();

            // redirect with message
            $this->session->flash('message', $this->trans('panel-admin/auth.messages.successRegistration'));
            return $this->redirect()->route('pa.signIn');
        }

        $auth->rollbackTransaction();

        return $this->view($view, [
            'data' => $data,
            'errors' => static::convertErrorMessage($auth->getMessages(), $this->trans()),
        ]);
    }

    protected function postSignIn($view, $data, $request)
    {
        /** @var Request $request */
        $email = $request->request->get('to', '');
        $password = $request->request->get('password', '');
        $rememberme = $request->request->getInt('rememberme', 0);

        $this->auth->beginTransaction();

        $user = $this->auth->signIn($email, $password);
        if($user) {
            // check confirmed email
            if($user->email_confirmed) {
                // write session
                $this->auth->createSession($user, (bool)$rememberme);

                $this->auth->commitTransaction();

                // redirect
                return $this->redirectStartPage();
            }
            else {
                // need confirme email
                $this->auth->addMessage('form', 'email_not_confirm');
            }
        }

        $this->auth->commitTransaction();

        // show error
        $data['data'] = ['email' => $email, 'rememberme'=>$rememberme];
        $data['errors'] = static::convertErrorMessage($this->auth->getMessages(), $this->trans());
        return $this->view($view, $data);
    }

    protected function redirectStartPage()
    {
        $config = $this->config('panel-admin.app.auth');

        $nameRoute = $this->auth->check($config['accessToBackend']) ?
            $config['nameRoute']['backend'] :
            $config['nameRoute']['home'];

        return $this->redirect()->route($nameRoute);
    }

    /**
     * @param string $view
     * @param string $subject
     * @param array $data
     */
    protected function sendEmail($view, $subject, $data, $mailer)
    {
        $to = new \stdClass;
        $to->email = $data['user']->email;
        $to->name = $data['user']->name;

        $data['u'] = $this->url();

        $mailer->to($to)
            ->send(new SendMail($view, $subject, $data));
    }
}
