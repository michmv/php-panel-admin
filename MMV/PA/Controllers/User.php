<?php

namespace MMV\PA\Controllers;

use Illuminate\Http\Request;
use MMV\PA\Controllers\BackendAbstract;
use MMV\Auth\Low\User as BaseUser;
use MMV\PA\Auth;
use MMV\PA\Session;
use MMV\PA\Auth\Locale;
use Illuminate\Database\DatabaseManager;
use MMV\PA\Access;
use MMV\PA\Auth\Security;

class User extends BackendAbstract
{
    protected function init()
    {
        $auth = $this->auth;
        $this->middleware(function($request, $next) use($auth) {
            if(!$auth->check('users.manager')) {
                $this->abort(403);
            }
            return $next($request);
        });
    }

    /**
     * List roles
     */
    public function roles(Access $access)
    {
        $defaultIdRole = (int)$this->config('panel-admin.app.defaultRole', 0);

        $roles = $access->getRoles();

        return $this->view('panel-admin.backend.roles', [
            'roleDefault' => $this->findRoleNameById($roles, $defaultIdRole),
            'roles' => $this->rolesToArray($roles),
        ]);
    }

    /**
     * Show role
     */
    public function showRole(Access $access, $id)
    {
        $role = $access->findRoleById((int)$id);
        if(!$role) return $this->abort(404);

        $resources = $access->getResourcesGroup();

        return $this->view('panel-admin.backend.role-show', [
            'role' => $role,
            'resources' => $resources,
            'access' => $access,
        ]);
    }

    /**
     * Show list users
     */
    public function list(DatabaseManager $db, Access $access)
    {
        $users = $db
            ->table('auth_users')
            ->select('id', 'email', 'name', 'role');

        return $this->view('panel-admin.backend.users', [
            'collection' => $users,
            'roles' => $this->simpleListRolesTitle($access->getRoles()),
        ]);
    }

    /**
     * Create user
     */
    public function create(Auth $auth, Access $access, Request $request, Session $session, Security $security)
    {
        $view = 'panel-admin.backend.users-create';
        $roles = $this->simpleListRolesTitle($access->getRoles());

        if($request->method() == 'POST')
            return $this->postCreate($auth, $request, $session, $security, $view, $roles);

        return $this->view($view, [
            'roles' => $roles,
        ]);
    }

    /**
     * Update user
     */
    public function update(Auth $auth, Access $access, Request $request, Session $session, $id)
    {
        $user = $auth->findUserById($id);
        if(!$user) return $this->abort(404);

        $view = 'panel-admin.backend.users-update';
        $roles = $this->simpleListRolesTitle($access->getRoles());

        if($request->method() == 'POST')
            return $this->postUpdate($auth, $request, $user, $session, $view, $roles);

        return $this->view($view, [
            'user' => $user,
            'roles' => $roles,
            'session' => $session,
        ]);
    }

    /**
     * Delete user by id
     */
    public function delete(Auth $auth, Session $session, $id)
    {
        $user = $auth->findUserById($id);
        if($user) {
            $auth->beginTransaction();
            $user->delete();
            $session->removeAllSessionForUser($user->id);
            $auth->commitTransaction();
        }
    }

    protected function postCreate(Auth $auth, Request $request, Session $session, Security $security, $view, $roles)
    {
        $data = $this->importData($request);
        $data['password'] = $security->randomString(16);
        $user = $auth->makeUser($data);

        if($user->save()) {
            // ok
            $session->flash('message', 'panel-admin/backend.messages.userCreated');
            return $this->redirect()->route('pa.users.update', [$user->id]);
        }
        else {
            // errors
            return $this->view($view, [
                'user' => $user,
                'roles' => $roles,
                'errors' => $this->convertErrorMessage($user->getMessages()),
            ]);
        }

    }

    protected function postUpdate(Auth $auth, Request $request, BaseUser $user, Session $session, $view, $roles)
    {
        $data = $this->importData($request);
        $user = $this->applyDataToUser($user, $data);

        $auth->beginTransaction();
        if($user->save()) {
            // ok
            $session->removeAllSessionForUser($user->id);
            $session->flash('message', 'panel-admin/backend.messages.userUpdated');
            $errors = [];
            $auth->commitTransaction();
        }
        else {
            // error
            $auth->rollbackTransaction();
            $errors = $this->convertErrorMessage($user->getMessages());
        }

        return $this->view($view, [
            'user' => $user,
            'roles' => $roles,
            'session' => $session,
            'errors' => $errors,
        ]);
    }

    protected function importData($request)
    {
        return [
            'email'           => $request->post('email', ''),
            'name'            => $request->post('name', ''),
            'email_confirmed' => (int)$request->post('email_confirmed', 0),
            'role'            => (int)$request->post('role', 0),
        ];
    }

    protected function applyDataToUser($user, $data)
    {
        foreach($data as $key => $val) {
            $user->$key = $val;
        }
        return $user;
    }

    protected function rolesToArray($roles)
    {
        $res = [];
        foreach($roles as $role) {
            $res[] = ['id'=>$role->id, 'name'=>$role->name, 'title'=>$role->title(), 'description'=>$role->description];
        }
        return $res;
    }

    protected function findRoleNameById($roles, $id)
    {
        foreach($roles as $role) {
            if($id == $role->id) return $role->name;
        }
        return 'unknown';
    }

    protected function convertErrorMessage(array $errors)
    {
        return Locale::makeMessageBag(Locale::trans($this->trans(), $errors));
    }

    protected function simpleListRolesTitle($roles)
    {
        $res = [];
        foreach($roles as $role) {
            $res[$role->id] = $this->trans($role->title());
        }
        return $res;
    }
}
