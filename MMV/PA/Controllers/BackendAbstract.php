<?php

namespace MMV\PA\Controllers;

use Illuminate\Foundation\Application;
use MMV\PA\Controllers\BaseAbstract;
use MMV\PA\Auth;
use MMV\PA\Helper;

abstract class BackendAbstract extends BaseAbstract
{
    public Auth $auth;

    public function __construct(Application $app, Auth $auth, Helper $helper)
    {
        $this->middleware(function($request, $next) use($auth) {
            if(!$auth->check('backend')) {
                $this->abort(403);
            }
            return $next($request);
        });

        $this->auth = $auth;
        parent::__construct($app, $helper);
    }
}
