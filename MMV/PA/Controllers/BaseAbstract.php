<?php

namespace MMV\PA\Controllers;

use Illuminate\Foundation\Application;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Routing\ControllerMiddlewareOptions;
use MMV\PA\Helper;

abstract class BaseAbstract
{
    /**
     * The middleware registered on the controller.
     *
     * @var array
     */
    protected $middleware = [];

    protected Helper $helper;

    protected Application $app;

    public function __construct(Application $app, Helper $helper)
    {
        $this->app = $app;
        $this->helper = $helper;

        $this->init();
    }

    /**
     * Init controller
     */
    protected function init()
    {
        // ...
    }

    /**
     * Register middleware on the controller.
     *
     * @param  \Closure|array|string  $middleware
     * @param  array  $options
     * @return \Illuminate\Routing\ControllerMiddlewareOptions
     */
    public function middleware($middleware, array $options = [])
    {
        foreach ((array) $middleware as $m) {
            $this->middleware[] = [
                'middleware' => $m,
                'options' => &$options,
            ];
        }

        return new ControllerMiddlewareOptions($options);
    }

    /**
     * Get the middleware assigned to the controller.
     *
     * @return array
     */
    public function getMiddleware()
    {
        return $this->middleware;
    }

    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string|null  $view
     * @param  \Illuminate\Contracts\Support\Arrayable|array  $data
     * @param  array  $mergeData
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function view($view = null, $data = [], $mergeData = [])
    {
        $factory = $this->app->make('view');
        $factory->share('h', $this->helper);
        $factory->share('u', $this->app->make('url'));

        if(func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($view, $data, $mergeData);
    }

    protected function url(): UrlGenerator
    {
        return $this->app->make('url');
    }

    /**
     * Get an instance of the redirector.
     *
     * @param  string|null  $to
     * @param  int  $status
     * @param  array  $headers
     * @param  bool|null  $secure
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    protected function redirect($to = null, $status = 302, $headers = [], $secure = null)
    {
        if (is_null($to)) {
            return $this->app->make('redirect');
        }

        return $this->app->make('redirect')->to($to, $status, $headers, $secure);
    }

    /**
     * Translate the given message.
     *
     * @param  string|null  $key
     * @param  array  $replace
     * @param  string|null  $locale
     * @return \Illuminate\Contracts\Translation\Translator|string|array|null
     */
    protected function trans($key = null, $replace = [], $locale = null)
    {
        if (is_null($key)) {
            return $this->app->make('translator');
        }

        return $this->app->make('translator')->get($key, $replace, $locale);
    }

    /**
     * Throw an HttpException with the given data.
     *
     * @param  \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Support\Responsable|int  $code
     * @param  string  $message
     * @param  array  $headers
     * @return void
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function abort($code, $message = '', array $headers = [])
    {
        if ($code instanceof Response) {
            throw new HttpResponseException($code);
        } elseif ($code instanceof Responsable) {
            throw new HttpResponseException($code->toResponse(request()));
        }

        $this->app->abort($code, $message, $headers);
    }

    /**
     * Get / set the specified configuration value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string|null  $key
     * @param  mixed  $default
     * @return mixed|\Illuminate\Config\Repository
     */
    protected function config($key = null, $default = null)
    {
        $config = $this->app->make('config');
        if(is_null($key)) {
            return $config;
        }

        if(is_array($key)) {
            return $config->set($key);
        }

        return $config->get($key, $default);
    }
}
