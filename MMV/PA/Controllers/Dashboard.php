<?php

namespace MMV\PA\Controllers;

use MMV\PA\Controllers\BackendAbstract;

class Dashboard extends BackendAbstract
{
    public function index()
    {
        return $this->view('panel-admin.dashboard');
    }
}
