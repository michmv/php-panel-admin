<?php

namespace MMV\PA\Widgets;

use MMV\PA\Widgets\TableGrid\StrategyInterface;
use MMV\PA\Utility\Widget;
use MMV\PA\Utility\Utility;

class TableGrid extends Widget
{
    public StrategyInterface $strategy;

    /**
     * Id for html
     * 
     * @var string
     */
    public $id = 'table-grid';

    /**
     * Class for div conteiner
     *
     * @var string
     */
    public $class = 'table-responsive-sm';

    /**
     * Limit rows on one page
     *
     * @var integer
     */
    public $limit = 0;

    /**
     * Collection
     *
     * @var mixed
     */
    public $collection = [];

    /**
     * If is value string to convert object. If array to check key `class`.
     *
     * @var array String, Array or Object
     */
    public $columns = [];

    /**
     * If is value string to convert object. If array to check key `class`.
     *
     * @var array String, Array or Object
     */
    public $filters = [];

    /**
     * Parameter for css class .col-sm-<int>
     * 
     * @var int
     */
    public $colSmNum = 4;

    /**
     * @var string
     */
    public $defaultColumn = \MMV\PA\Widgets\TableGrid\Column::class;

    /**
     * @var string
     */
    public $defautlFilter = \MMV\PA\Widgets\TableGrid\Filter::class;

    /**
     * Params for href
     *
     * @var array
     */
    public $params = [];

    /**
     * Name template for render
     *
     * @var string
     */
    public $view = 'panel-admin.widgets.table_grid';

    /**
     * More resources for table grid
     *
     * @var array
     */
    public $resources = [];

    public function __toString()
    {
        $columns = Utility::toObjectList($this->columns, $this->defaultColumn);
        $filters = Utility::toObjectList($this->filters, $this->defautlFilter);

        $import = $_GET;
        $params = $this->params;

        list($params, $filters) = $this->applyFilter($filters, $params, $import);
        $params = $this->applySort($columns, $params, $import);
        list($total, $select, $params) = $this->applySlice($params, $import);

        $collection = $this->strategy->get($this->collection);

        return $this->view($this->view, [
            'it' => $this,
            'collection' => $collection,
            'columns' => $columns,
            'filters' => $filters,
            'params' => $this->cleanEmptyParams($params),
            'paginator' => ['total'=>$total, 'select'=>$select],
            'resources' => $this->compileResources($this->compileResources($this->resources, $filters), $columns),
        ]);
    }

    protected function applyFilter($filters, $params, $import)
    {
        foreach($filters as $index => $filter) {
            if(array_key_exists('f'.$index, $import)) {
                $params['f'.$index] = trim($import['f'.$index]);
                [$this->collection, $filter] = $this->strategy->filter($this->collection, $filter, $params['f'.$index]);
                $filters[$index] = $filter;
            }
        }
        return [$params, $filters];
    }

    protected function applySort($columns, $params, $import)
    {
        if(array_key_exists('s', $import)) {
            if(array_key_exists($import['s'], $columns) && ($columns[$import['s']])->sort) {
                $params['s'] = (int)$import['s'];
                $t = (int)($import['t'] ?? 0);
                if(in_array($t, [0, 1])) {
                    $params['t'] = $t;
                }
                else {
                    $params['t'] = 0;
                }
                $this->collection = $this->strategy->sort($this->collection, $columns[$params['s']], $params['t']);
            }
        }
        return $params;
    }

    protected function applySlice($params, $import)
    {
        if($this->limit == 0) {
            $total = 1;
            $select = 1;
        }
        else {
            $total = ceil($this->strategy->count($this->collection) / $this->limit);

            $select = (int)($import['p'] ?? 1);
            if($select < 1 || $select > $total) $select = 1;

            if($select > 1) $params['p'] = $select;

            $this->collection = $this->strategy->slice($this->collection, $select, $this->limit);
        }
        return [$total, $select, $params];
    }

    protected function cleanEmptyParams($params)
    {
        foreach($params as $key => $val) {
            if($val === '') unset($params[$key]);
        }
        return $params;
    }

    protected function compileResources($source, $add)
    {
        foreach($add as $it) {
            $res = $it->resources($this->helper, $this->id);
            if($res) $source = array_merge($source, $res);
        }
        return $source;
    }
}
