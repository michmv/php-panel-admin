<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;

class SimpleAntiSpam extends Widget
{
    public $id = '';

    public function __toString()
    {
        return $this->view('panel-admin.widgets.simple-anti-spam', ['it'=>$this]);
    }
}
