<?php

namespace MMV\PA\Widgets\Utility;

use Illuminate\Support\MessageBag;

trait ErrorsTrait
{
    /**
     * @var MessageBag|null|array
     */
    public $errors;

    protected function getErrors($name='')
    {
        if(!$this->errors) return [];

        if(is_array($this->errors))
            return $this->errors[$name] ?? [];

        if(is_object($this->errors) && $this->errors->has($this->name))
            return $this->errors->get($name);

        return [];
    }
}
