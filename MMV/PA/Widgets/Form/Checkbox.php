<?php

namespace MMV\PA\Widgets\Form;

use MMV\PA\Widgets\Form\Input;

class Checkbox extends Input
{
    public $view = 'panel-admin.widgets.form.checkbox';

    public function __toString()
    {
        $this->type = 'checkbox';

        return parent::__toString();
    }

    protected function getValue()
    {
        if(is_object($this->value)) {
            $property = $this->name;
            return ($this->value->$property ?? '');
        }
        else if(is_array($this->value)) {
            return ($this->value[$this->name] ?? '');
        }
        else {
            $res = (string)$this->value;
        }

        return $res;
    }
}
