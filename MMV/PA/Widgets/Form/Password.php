<?php

namespace MMV\PA\Widgets\Form;

use MMV\PA\Widgets\Form\Input;

class Password extends Input
{
    public function __toString()
    {
        $this->type = 'password';
        $this->value = '';

        return parent::__toString();
    }
}
