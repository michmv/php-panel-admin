<?php

namespace MMV\PA\Widgets\Form;

use MMV\PA\Widgets\Form\Input;

class Select extends Input
{
    /**
     * @var string
     */
    public $disableSelect = '';

    /**
     * @var string[]
     */
    public $options = [];

    public $view = 'panel-admin.widgets.form.select';

    public function __toString()
    {
        return $this->view($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
            'value' => $this->getValue(),
        ]);
    }
}
