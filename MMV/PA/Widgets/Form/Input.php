<?php

namespace MMV\PA\Widgets\Form;

use MMV\PA\Utility\Widget;
use MMV\PA\Widgets\Utility\ErrorsTrait;

class Input extends Widget
{
    use ErrorsTrait;

    public $name = 'field';

    public $alias = '';

    public $label = '';

    public $type = 'text';

    public $required = false;

    public $autofocus = false;

    public $disabled = false;

    public $placeholder = '';

    public $help = '';

    /**
     * @var mixed
     */
    public $value;

    public $default = '';

    public $view = 'panel-admin.widgets.form.input';

    public function __toString()
    {
        $t = $this->getValue();
        return $this->view($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
            'value' => $this->getValue(),
        ]);
    }

    public function name()
    {
        return $this->alias ? $this->alias : $this->name;
    }

    protected function getValue()
    {
        if(!$this->value) return $this->default;

        if(is_object($this->value)) {
            $property = $this->name;
            return ($this->value->$property ?? '');
        }
        else if(is_array($this->value)) {
            return ($this->value[$this->name] ?? '');
        }
        else {
            $res = (string)$this->value;
        }

        return $res;
    }
}
