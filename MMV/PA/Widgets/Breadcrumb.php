<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;

class Breadcrumb extends Widget
{
    /**
     * @var string|string[]
     */
    public $path = [];

    public function __toString()
    {
        $res = [];

        $n = 0;
        $last = count($this->path) - 1;
        foreach($this->path as $point) {
            if($n != $last) {
                $res[] = $this->htmlLink($point[1], $point[0]);
            } else {
                $res[] = $this->htmlLast($point[0]);
            }
            $n++;
        }

        return $this->htmlOl($res);
    }

    /**
     * @param string[] $inner
     * @return void
     */
    protected function htmlOl($inner)
    {
        return ($inner) ?
            '<ol class="breadcrumb">'.implode('', $inner).'</ol>' :
            '';
    }

    /**
     * @param string $url
     * @param string $title
     * @return string
     */
    protected function htmlLink($url, $title)
    {
        return '<li class="breadcrumb-item"><a href="'.$url.'">'.$title.'</a></li>';
    }

    /**
     * @param string $title
     * @return string
     */
    protected function htmlLast($title)
    {
        return '<li class="breadcrumb-item active">'.$title.'</li>';
    }
}
