<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;

class Alert extends Widget
{
    /**
     * primary, secondary, success, danger, warning, info, light, dark
     * 
     * @var string
     */
    public $type = "dark";

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string|array
     */
    public $message;

    public $view = 'panel-admin.widgets.alert';

    public function __toString()
    {
        if(!$this->message) return '';

        if(is_array($this->message)) {
            $messages = $this->message;
        } else {
            $messages = [(string)$this->message];
        }

        return $this->view($this->view, [
            'it' => $this,
            'messages' => $messages,
        ]);
    }
}
