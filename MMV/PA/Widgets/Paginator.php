<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;
use MMV\PA\Utility\Utility;
use stdClass;

class Paginator extends Widget
{
    /**
     * @var integer
     */
    public $total = 1;

    /**
     * @var integer
     */
    public $select = 1;

    /**
     * @var string
     */
    public $url = '';

    /**
     * @var string
     */
    public $name = 'p';

    /**
     * More parameters for url
     * 
     * @var string[]
     */
    public $params = [];

    /**
     * @var integer
     */
    public $cells = 3;

    public function __toString()
    {
        if($this->total < 2) return ''; // empty

        $links = [];

        // select
        $select = ($this->select) ? $this->select : (int)($_GET[$this->name] ?? 1);
        if($select < 1) $select = 1;
        if($select > $this->total) $select = $this->total;

        // prev
        $prev = $this->getLink($this->genUrl($select - 1), $select - 1);
        if($select <= 1) $prev->disable = true;

        // first
        $links[] = $this->getLink($this->genUrl(1), 1);

        if($this->total <= ($this->cells * 2 + 1)) {
            $periodStart = 2;
            $periodFinish = $this->total - 1;
        } else {
            $periodStart = $select - $this->cells;
            $periodFinish = $select + $this->cells;
            if($periodStart <= 2) {
                $periodStart = 2;
                $periodFinish = 2 + ($this->cells * 2) - 1;
            }
            if($periodFinish >= $this->total - 1) {
                $periodFinish = $this->total - 1;
                $periodStart = $periodFinish - ($this->cells * 2) + 1;
            }
        }

        // left skip
        if($periodStart > 2) {
            $links[] = $this->getLink('#', '...', true);
        }

        // links
        for($i=$periodStart; $i<=$periodFinish; $i++) {
            $links[] = $this->getLink($this->genUrl($i), $i);
        }

        // right skip
        if($periodFinish < $this->total - 1) {
            $links[] = $this->getLink('#', '...', true);
        }

        // last
        $links[] = $this->getLink($this->genUrl($this->total), $this->total);

        // next
        $next = $this->getLink($this->genUrl($select + 1), $select + 1);
        if($select >= $this->total) $next->disable = true;

        return (string)$this->view('panel-admin.widgets.paginator', [
            'next' => $next,
            'prev' => $prev,
            'links' => $links,
            'total' => $this->total,
            'select' => $select,
        ]);
    }

    /**
     * @param int $n
     * @return string
     */
    public function genUrl($n)
    {
        if($n != 1) $params = array_merge($this->params, [$this->name => $n]);
        else {
            $params = $this->params;
            unset($params[$this->name]);
        }

        $url = ($this->url) ? $this->url : $this->helper->url->current();

        return Utility::urlPlusParams($url, $params);
    }

    public static function urlPlusParams($url, $params)
    {
        $res = [];

        foreach($params as $name => $value) {
            $res[] = $name . '=' . urldecode($value);
        }

        return $url . (($res) ? '?' . implode('&', $res) : '');
    }

    protected function getLink($url='#', $n='...', $disable=false)
    {
        $o = new stdClass();
        $o->url = $url;
        $o->n = $n;
        $o->disable = $disable;
        return $o;
    }
}
