<?php

namespace MMV\PA\Widgets\TableGrid;

class Filter
{
    /**
     * Name field
     *
     * @var string
     */
    public $field = '';

    /**
     * Title for show
     *
     * @var string
     */
    public $title = '';

    /**
     * Set True if  it filter apply for collection, False if filter not have value.
     *
     * @var boolean
     */
    public $active = false;

    public function __construct($field='')
    {
        $this->field = $field;
    }

    public function htmlCol($helper, $index, $params, $colSmNum)
    {
        return '<div class="input-group input-group-sm mb-3 col-sm-'.$colSmNum.'">'.
                    '<div class="input-group-prepend">'.
                        '<span class="input-group-text">'.$this->getTitle($helper).'</span>'.
                    '</div>'.
                    '<input type="text" class="form-control" name="f'.$index.'" value="'.$helper->e($params['f'.$index] ?? '').'">'.
                '</div>';
    }

    public function resources($helper, $id)
    {
        return [];
    }

    /**
     * @param \MMV\PA\Helper $helper
     * @return string
     */
    protected function getTitle($helper)
    {
        if($this->title) {
            $title = $this->title;
        }
        else {
            $fc = mb_strtoupper(mb_substr($this->field, 0, 1));
            $title = $fc.mb_substr($this->field, 1);
        }
        return $helper->e($title);
    }
}
