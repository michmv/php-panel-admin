<?php

namespace MMV\PA\Widgets\TableGrid;

use MMV\PA\Utility\Utility;
use MMV\PA\Widgets\TableGrid\Column;

class ColumnControl extends Column
{
    /**
     * @var mixed[]
     */
    public $buttons = [];

    /**
     * @var string
     */
    public $defaultButton = \MMV\PA\Types\TableGridButton::class;

    /**
     * @param \MMV\PA\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlCellBody($helper, $row, $index)
    {
        $buttons = Utility::toObjectList($this->buttons, $this->defaultButton);

        $res = [];
        foreach($buttons as $button) {
            $res[] = $button->htmlButton($helper, $row, $index);
        }

        return '<td width="1px" class="n'.$index.'">'.($res ? '<span class="text-nowrap buttons-block">'.implode('', $res).'</span>' : '' ).'</td>';
    }
}
