<?php

namespace MMV\PA\Widgets\TableGrid;

use MMV\PA\Utility\Utility;

class Column
{
    /**
     * Name field
     *
     * @var string
     */
    public $field = '';

    /**
     * Title for show
     *
     * @var string
     */
    public $title = '';

    /**
     * Active translate
     *
     * @var boolean
     */
    public $translate = false;

    /**
     * @var boolean
     */
    public $escape = true;

    /**
     * Active sort function for this column
     *
     * @var boolean
     */
    public $sort = false;

    public function __construct($field='')
    {
        $this->field = $field;
    }

    /**
     * @param \MMV\PA\Helper $helper
     * @param int $index
     * @param array $params
     * @return string
     */
    public function htmlCellHead($helper, $index, $params)
    {
        return '<th class="n'.$index.'">'.$this->getTitle($helper, $index, $params).'</th>';
    }

    /**
     * @param \MMV\PA\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlCellBody($helper, $row, $index)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->t($field);
        if($this->escape) $field = $helper->e($field);
        return '<td class="n'.$index.'">'.$field.'</td>';
    }

    public function resources($helper, $id)
    {
        return [];
    }

    /**
     * @param \MMV\PA\Helper $helper
     * @param int $index
     * @param array $params
     * @return string
     */
    protected function getTitle($helper, $index, $params)
    {
        if($this->title) {
            $title = $this->title;
        }
        else {
            $fc = mb_strtoupper(mb_substr($this->field, 0, 1));
            $title = $fc.mb_substr($this->field, 1);
        }
        $title = $helper->e($title);

        // no sort
        if(!$this->sort) return $title;

        if(array_key_exists('s', $params) && $index == $params['s']) {
            if($params['t'] == 1) $icon = 'sort-up';
            else $icon = 'sort-down';
            $params['t'] = ($params['t'] == 0) ? 1 : 0;
        }
        else {
            // default
            $icon = 'sort';
            $params['s'] = $index;
            $params['t'] = 0;
        }

        $url = Utility::urlPlusParams($helper->url->current(), $params);
        return '<a class="text-white" href="'.$url.'">'.$title.' <i class="fas fa-'.$icon.'"></i></a>';
    }
}
