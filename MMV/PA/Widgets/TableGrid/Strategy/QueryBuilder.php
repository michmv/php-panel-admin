<?php

namespace MMV\PA\Widgets\TableGrid\Strategy;

use MMV\PA\Widgets\TableGrid\StrategyInterface;

class QueryBuilder implements StrategyInterface
{
    /**
     * @param mixed $collection
     * @return object[]
     */
    public function get($collection)
    {
        return $collection->get();
    }

    /**
     * @param mixed $collection
     * @return int
     */
    public function count($collection)
    {
        return $collection->count();
    }

    /**
     * @param mixed $collection
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function slice($collection, $page, $limit)
    {
        if($limit != 0) {
            $collection
                ->offset(($page - 1) * $limit)
                ->limit($limit);
        }
        //
        return $collection;
    }

    /**
     * @param mixed $collection
     * @param object $column
     * @param int $type 0 is ASC, 1 is DESC
     * @return mixed
     */
    public function sort($collection, $column, $type)
    {
        if(method_exists($column, 'sort')) return $column->sort($collection, $type);

        return $collection->orderBy($column->field, $type ? 'desc' : 'asc');
    }

    /**
     * @param mixed $collection
     * @param object $filter
     * @param string $value
     * @return mixed [mixed $collection, mixed $filter]
     */
    public function filter($collection, $filter, $value)
    {
        if(method_exists($filter, 'filter')) return $filter->filter($collection, $value);

        if($value !== '') {
            $filter->active = true;
            $field = $filter->field;

            $collection->where($field, 'like', '%'.$value.'%');
        }

        return [$collection, $filter];
    }
}
