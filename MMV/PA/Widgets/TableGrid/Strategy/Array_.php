<?php

namespace MMV\PA\Widgets\TableGrid\Strategy;

use MMV\PA\Widgets\TableGrid\StrategyInterface;

class Array_ implements StrategyInterface
{
    /**
     * @param mixed $collection
     * @return object[]
     */
    public function get($collection)
    {
        $res = [];
        foreach($collection as $i) {
            $res[] = (object)$i;
        }
        return $res;
    }

    /**
     * @param mixed $collection
     * @return int
     */
    public function count($collection)
    {
        return count($collection);
    }

    /**
     * @param mixed $collection
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function slice($collection, $page, $limit)
    {
        if($limit != 0) {
            $collection = array_slice($collection, ($page - 1) * $limit, $limit);
        }
        return $collection;
    }

    /**
     * @param mixed $collection
     * @param object $column
     * @param int $type 0 is ASC, 1 is DESC
     * @return mixed
     */
    public function sort($collection, $column, $type)
    {
        if(method_exists($column, 'sort')) return $column->sort($collection, $type);

        $field = $column->field;
        if(!$type) {
            $f = function($a, $b) use($field) {
                if($a[$field] == $b[$field]) return 0;
                return ($a[$field] < $b[$field]) ? -1 : 1;
            };
        }
        else {
            $f = function($a, $b) use($field) {
                if($a[$field] == $b[$field]) return 0;
                return ($a[$field] > $b[$field]) ? -1 : 1;
            };
        }
        usort($collection, $f);
        return $collection;
    }

    /**
     * @param mixed $collection
     * @param object $column
     * @param string $value
     * @return mixed [mixed $collection, mixed $filter]
     */
    public function filter($collection, $filter, $value)
    {
        if(method_exists($filter, 'filter')) return $filter->filter($collection, $value);

        if($value !== '') {
            $filter->active = true;
            $field = $filter->field;

            $func = function($var) use($value, $field) {
                return strpos($var[$field], $value) !== false;
            };

            $collection = array_filter($collection, $func);
        }

        return [$collection, $filter];
    }
}
