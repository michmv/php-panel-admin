<?php

namespace MMV\PA\Widgets\TableGrid\QueryBuilder;

use MMV\PA\Widgets\TableGrid\QueryBuilder\FilterEqual;

class FilterSelect extends FilterEqual
{
    /**
     * @var string
     */
    public $disableSelect = '-';

    /**
     * List
     * 
     * @var string[]
     */
    public $options = [];

    public function htmlCol($helper, $index, $params, $colSmNum)
    {
        $options = $this->disableSelect ?
            ['' => $this->disableSelect] + $this->options :
            $this->options;

        $value = $params['f'.$index] ?? '';

        $res = [];
        foreach($options as $key => $val) {
            $selected = ((string)$key === (string)$value) ? ' selected' : '';
            $res[] = '<option'.$selected.' value="'.$helper->e($key).'">'.$helper->e($val).'</option>';
        }

        return 
            '<div class="input-group input-group-sm mb-3 col-sm-'.$colSmNum.'">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="filterSelect'.$index.'">'.$this->getTitle($helper).'</label>
                </div>
                <select name="f'.$index.'" class="custom-select filter-select" id="filterSelect'.$index.'">'.implode('', $res).'</select>
            </div>';
    }

    public function resources($helper, $id)
    {
        $res =
            '<script>
                panelAdmin.addInitFunction(function(){
                    $("#'.$id.' .filter-select").on("change", function(){
                        var form = $(this).parents("form")
                        form.submit()
                    })
                })
            </script>'
            ;
        return ['SelectFilter' => $res];
    }
}
