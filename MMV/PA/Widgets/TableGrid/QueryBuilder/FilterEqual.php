<?php

namespace MMV\PA\Widgets\TableGrid\QueryBuilder;

use MMV\PA\Widgets\TableGrid\Filter;

class FilterEqual extends Filter
{
    /**
     * @param mixed $collection
     * @param string $value
     * @return mixed [mixed $collection, mixed $filter]
     */
    public function filter($collection, $value)
    {
        if($value !== '') {
            $this->active = true;
            $field = $this->field;

            $collection->where($field, '=', $value);
        }

        return [$collection, $this];
    }
}
