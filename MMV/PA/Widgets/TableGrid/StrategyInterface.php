<?php

namespace MMV\PA\Widgets\TableGrid;

interface StrategyInterface
{
    /**
     * @param mixed $collection
     * @return object[]
     */
    public function get($collection);

    /**
     * @param mixed $collection
     * @return int
     */
    public function count($collection);

    /**
     * @param mixed $collection
     * @param int $page
     * @param int $limit
     * @return mixed
     */
    public function slice($collection, $page, $limit);

    /**
     * @param mixed $collection
     * @param object $column
     * @param int $type 0 is ASC, 1 is DESC
     * @return mixed
     */
    public function sort($collection, $column, $type);

    /**
     * @param mixed $collection
     * @param object $column
     * @param string $value
     * @return mixed [mixed $collection, mixed $filter]
     */
    public function filter($collection, $filter, $value);
}
