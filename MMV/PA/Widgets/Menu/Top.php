<?php

namespace MMV\PA\Widgets\Menu;

use MMV\PA\Utility\Widget;

class Top extends Widget
{
    public $path = 'panel-admin.top-menu';

    public function __toString()
    {
        $res = [];
        $config = $this->helper->c($this->path);
        foreach($config as $item) {
            if($this->access($item->resources)) {
                if($item->separator) {
                    $res[] = 'separator';
                }
                else {
                    $link = $this->urlGenerate($item->link);
                    $title = $this->helper->t($item->title);
                    $res[] = '<li>'.$this->genLink($title, $link, $item->newTab).'</li>';
                }
            }
        }
        $res = $this->separatorToTag($this->filterSeparator($res));
        return $res ? '<ul class="scroll">'.implode('', $res).'</ul>' : '';
    }

    protected function genLink($title, $link, $newTab)
    {
        $icon = $target = '';
        if($newTab) {
            $icon = ' <i class="fas fa-external-link-alt"></i>';
            $target = ' target="_blank"';
        }
        return '<a'.$target.' href="'.$link.'" title="'.$title.'">'.$title.$icon.'</a>';
    }

    protected function separatorToTag($items)
    {
        foreach($items as $key => $val) {
            if($val == 'separator')
                $items[$key] = '<li><span class="pat-separation"></span></li>';
        }
        return $items;
    }

    protected function filterSeparator($items)
    {
        $res = [];

        // remove dublicate
        $separator = false;
        foreach($items as $item) {
            if($item == 'separator') {
                if(!$separator) {
                    $res[] = $item;
                    $separator = true;
                }
            }
            else {
                $res[] = $item;
                $separator = false;
            }
        }

        if(!$res) return [];

        // remove first and last separator
        if($res[count($res)-1] == 'separator') unset($res[count($res)-1]);
        if(isset($res[0]) && $res[0] == 'separator') unset($res[0]);

        return $res;
    }

    protected function access($resources)
    {
        if($resources == '' || $resources == []) return true;

        return $this->helper->auth()->check($resources);
    }

    protected function urlGenerate($link)
    {
        if(is_string($link)) $link = ['route', $link];

        return call_user_func_array([$this->helper->url, $link[0]], array_slice($link, 1));
    }
}
