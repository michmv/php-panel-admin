<?php

namespace MMV\PA\Widgets\Menu;

use MMV\PA\Widgets\Menu\Top;

class Left extends Top
{
    public $path = 'panel-admin.left-menu';

    public $select = '';

    public function __toString()
    {
        $config = $this->helper->c($this->path);
        return $this->htmlMenu($config, 0);
    }

    /**
     * @param MenuItemLeft[] $list
     * @param int $depth
     * @return string
     */
    protected function htmlMenu($list, $depth)
    {
        $res = [];
        foreach($list as $item) {
            if($this->access($item->resources)) {
                if($item->separator) {
                    $res[] = 'separator';
                }
                else {
                    if($item->child) {
                        // submenu
                        $sub = $this->htmlMenu($item->child, $depth+1);
                        if($sub) {
                            $title = $this->helper->t($item->title);
                            $res[] = '<li class="pal-sub"><span class="pal-text">'.$title.'</span>'.$sub.'</li>';
                        }
                    }
                    else {
                        // menu
                        $link = $this->urlGenerate($item->link);
                        $title = $this->helper->t($item->title);
                        $select = $item->title == $this->select && $this->select != '';
                        $res[] = '<li>'.$this->genLinkEx($title, $link, $item->newTab, $select).'</li>';
                    }
                }
            }
        }
        $res = $this->separatorToTag($this->filterSeparator($res));
        return $res ? '<ul'.($depth == 0 ? ' class="pal-menu"' : '').'>'
            .implode('', $res).'</ul>' : '';
    }

    protected function separatorToTag($items)
    {
        foreach($items as $key => $val) {
            if($val == 'separator')
                $items[$key] = '<li><span class="pal-separation pal-text"><span></span></span></li>';
        }
        return $items;
    }

    protected function genLinkEx($title, $link, $newTab, $select)
    {
        $icon = $target = '';
        if($newTab) {
            $icon = ' <i class="fas fa-external-link-alt"></i>';
            $target = ' target="_blank"';
        }
        return '<a'.$target.' class="pal-text'.($select ? ' pal-select' : '').'" href="'.$link.'" title="'.$title.'">'.$title.$icon.'</a>';
    }
}
