<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;

class Header extends Widget
{
    public $view = 'panel-admin.widgets.header';

    public function __toString()
    {
        $res = [];
        foreach($this->helper->header->getResources() as $key => $resource) {
            if(is_string($resource)) {
                $res[] = $resource;
            }
            else if(is_object($resource)) {
                $res[] = $resource->render($this->helper->url);
            }
            else {
                $res[] = '<!-- Unknown resouse: '.$key.' -->';
            }
        }
        return $this->view($this->view, [
            'it' => $this->helper->header,
            'resources' => $res,
        ]);
    }
}
