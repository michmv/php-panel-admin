<?php

namespace MMV\PA\Widgets;

use MMV\PA\Utility\Widget;
use MMV\PA\Widgets\Utility\ErrorsTrait;

class Form extends Widget
{
    use ErrorsTrait;

    public $id = '';

    public $action = '';

    public $method = 'post';

    public $enctype = 'multipart/form-data';

    public $name = 'form';

    public $view = 'panel-admin.widgets.form';

    public $antispam = false;

    public function __toString()
    {
        if(!$this->action) $this->action = $this->helper->url->current();

        return $this->view($this->view, [
            'it' => $this,
            'errors' => $this->getErrors($this->name),
        ]);
    }
}
