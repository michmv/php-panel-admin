<?php

namespace MMV\PA;

use Illuminate\Foundation\Application;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use MMV\PA\Session;
use MMV\PA\Auth;
use MMV\PA\Utility\Header;
use MMV\PA\Utility\Utility;
use Illuminate\Translation\Translator;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Contracts\Support\DeferringDisplayableValue;
use Illuminate\Contracts\Support\Htmlable;

class Helper
{
    public Header $header;

    public UrlGenerator $url;

    public $selectBackendMenuLeft = '';

    protected Application $app;

    protected ?Translator $translator=null;

    protected ?Auth $auth = null;

    protected ?Session $session = null;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->header = new Header($this->c('panel-admin.app.header'));
        $this->url = $app->make('url');
    }

    /**
     * Get the translation for the given key.
     *
     * @param  string  $key
     * @param  array  $replace
     * @param  string|null  $locale
     * @param  bool  $fallback
     * @return string|array
     */
    public function t($key, array $replace = [], $locale = null, $fallback = true)
    {
        if(is_null($this->translator)) {
            $this->translator = $this->app->make('translator');
        }
        return $this->translator->get($key, $replace, $locale, $fallback);
    }

    /**
     * Config application
     * 
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function c($name, $default=null)
    {
        return $this->app->make('config')->get($name, $default);
    }

    public function csrf_token()
    {
        $session = $this->session();

        $time = time();
        $name = $this->c('panel-admin.app.csrf.token_name');
        $name_time_life = $name.'_time_life';

        $token = $session->get($name, '');

        // check token time_life
        if(!$token || $time > $session->get($name_time_life, 0)) {
            // create new token
            $ttl = $this->c('panel-admin.app.csrf.token_time_life');
            $token = Str::random(40);
            $session->set($name_time_life, $time + $ttl);
            $session->set($name, $token);
        }

        return $token;
    }

    public function csrf_name()
    {
        return $this->c('panel-admin.app.csrf.token_name');
    }

    public function csrf_field()
    {
        return new HtmlString('<input type="hidden" name="'.$this->csrf_name().'" value="'.$this->csrf_token().'">');
    }

    public function widget(string $class, array $parameters=[])
    {
        $factory = $this->app->make('view');
        $widget = new $class($factory, $this);
        return Utility::configurator($widget, $parameters);
    }

    public function getLocale()
    {
        return $this->app->getLocale();
    }

    /**
     * @return Auth
     */
    public function auth()
    {
        if(is_null($this->auth)) {
            $this->auth = $this->app->make(Auth::class);
        }
        return $this->auth;
    }

    /**
     * @return Session
     */
    public function session()
    {
        if(is_null($this->session)) {
            $this->session = $this->app->make(Session::class);
        }
        return $this->session;
    }

    /**
     * Encode HTML special characters in a string.
     *
     * @param  \Illuminate\Contracts\Support\DeferringDisplayableValue|\Illuminate\Contracts\Support\Htmlable|string  $value
     * @param  bool  $doubleEncode
     * @return string
     */
    function e($value, $doubleEncode = true)
    {
        if ($value instanceof DeferringDisplayableValue) {
            $value = $value->resolveDisplayableValue();
        }

        if ($value instanceof Htmlable) {
            return $value->toHtml();
        }

        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', $doubleEncode);
    }

    public function getSection($name)
    {
        return $this->app->view->getSection($name);
    }
}
