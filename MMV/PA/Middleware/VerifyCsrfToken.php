<?php

namespace MMV\PA\Middleware;

use Closure;
use Illuminate\Http\Request;
use MMV\PA\Session;
use Illuminate\Foundation\Application;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken
{
    protected Session $session;

    protected Application $app;

    public function __construct(Session $session, Application $app)
    {
        $this->session = $session;
        $this->app = $app;
    }

    public function handle(Request $request, Closure $next)
    {
        if(!in_array($request->method(), ['HEAD', 'GET', 'OPTIONS'])) {
            $config = $this->app->make('config')->get('panel-admin.app.csrf');

            $token = $request->post($config['token_name'], '');
            $verify = $this->session->get($config['token_name'], '');

            if(!($token && $verify && $token === $verify)) {
                throw new TokenMismatchException('CSRF token mismatch.');
            }
        }

        return $next($request);
    }
}
