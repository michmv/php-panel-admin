<?php

namespace MMV\PA\Middleware;

use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

class SimpleAntiSpam
{
    protected ResponseFactory $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public function handle(Request $request, Closure $next)
    {
        if( $request->method() == 'POST' &&
            (   (string)$request->post('email', '_') !== '' ||
                (string)$request->post('promise', '_') !== 'I am human'
            )
        ) {
            throw new HttpResponseException($this->responseFactory->view('panel-admin.spam'));
        }

        return $next($request);
    }
}
