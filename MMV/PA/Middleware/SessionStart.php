<?php

namespace MMV\PA\Middleware;

use Closure;
use MMV\PA\Session;

class SessionStart
{
    protected Session $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // save session if need
        $this->session->save();

        return $response;
    }
}
