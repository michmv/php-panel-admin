<?php

namespace MMV\PA\Types;

class TableGridButton
{
    /**
     * @var string
     */
    public $id = '';

    /**
     * @var string
     */
    public $icon = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var callable
     */
    public $url;

    public function __construct($icon='')
    {
        $this->icon = $icon;
        $this->url = function() { return '#'; };
    }

    public function htmlButton($helper, $row, $index)
    {
        $title = $this->title ? ' title="'.$helper->e($this->title).'"' : '';
        $id = $this->id ? ' class="'.$this->id.'"' : '';

        return '<a'.$title.$id.' data-id="'.$row->id.'" href="'.call_user_func_array($this->url, [$helper, $row, $index]).'"><i class="far '.$this->icon.'"></i></a>';
    }
}
