<?php

namespace MMV\PA\Types;

use Illuminate\Routing\UrlGenerator;

class ResourceJs
{
    public $type = '';

    /**
     * @var string|array
     */
    public $src = '#';

    public $integrity = '';

    public $crossorigin = '';

    /**
     * @param string|array $src
     * @param string $integrity
     * @param string $crossorigin
     * @param string $type
     */
    public function __construct($src, $integrity='', $crossorigin='', $type='text/javascript')
    {
        $this->src = $src;
        $this->type = $type;
        $this->integrity = $integrity;
        $this->$crossorigin = $crossorigin;
    }

    public function render(UrlGenerator $url)
    {
        $attrs = [];
        foreach(get_object_vars($this) as $name => $t) {
            if($this->$name) {
                $attrs[] = $name . '="' . (($name == 'src') ?
                        $this->genUrl($url, $this->$name) :
                        $this->$name ) .
                    '"';
            }
        }
        return '<script'.($attrs ? ' '.implode(' ', $attrs) : '').'></script>';
    }

    protected function genUrl($url, $src)
    {
        if(is_string($src)) return $src;

        $func = $src[0];
        return call_user_func_array([$url, $func], array_splice($src, 1));
    }
}
