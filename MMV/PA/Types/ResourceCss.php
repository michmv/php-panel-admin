<?php

namespace MMV\PA\Types;

use Illuminate\Routing\UrlGenerator;

class ResourceCss
{
    public $rel = '';

    public $href = '#';

    public $integrity = '';

    public $crossorigin = '';

    public $media = '';

    /**
     * @param string|array $src
     * @param string $integrity
     * @param string $crossorigin
     * @param string $type
     */
    public function __construct($href, $integrity='', $crossorigin='', $media='', $rel='stylesheet')
    {
        $this->href = $href;
        $this->integrity = $integrity;
        $this->$crossorigin = $crossorigin;
        $this->media = $media;
        $this->rel = $rel;
    }

    public function render(UrlGenerator $url)
    {
        $attrs = [];
        foreach(get_object_vars($this) as $name => $t) {
            if($this->$name) {
                $attrs[] = $name . '="' . (($name == 'href') ?
                        $this->genUrl($url, $this->$name) :
                        $this->$name ) .
                    '"';
            }
        }
        return '<link'.($attrs ? ' '.implode(' ', $attrs) : '').'>';
    }

    protected function genUrl($url, $src)
    {
        if(is_string($src)) return $src;

        $func = $src[0];
        return call_user_func_array([$url, $func], array_splice($src, 1));
    }
}

