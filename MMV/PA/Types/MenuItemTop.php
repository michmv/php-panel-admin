<?php

namespace MMV\PA\Types;

class MenuItemTop
{
    public $title = '';

    /**
     * @var string|array
     */
    public $link = '';

    /**
     * @var string|array
     */
    public $resources = '';

    public bool $newTab = false;

    public bool $separator = false;

    /**
     * @param string $title
     * @param string|array $link
     * @param string|array $resources
     * @param bool $newTab
     * @param bool $separator
     */
    public function __construct(string $title, $link='', $resources='', bool $newTab=false, bool $separator=false)
    {
        $this->title = $title;
        $this->link = $link;
        $this->resources = $resources;
        $this->newTab = $newTab;
        $this->separator = $separator;
    }

    /**
     * Create menu item
     * 
     * @param string $title
     * @param string|array $link
     * @param string|array $resources
     * @param bool $newTab
     * @return self
     */
    public static function item($title, $link, $resources='', $newTab=false)
    {
        $class = get_called_class();
        return new $class($title, $link, $resources, $newTab);
    }

    /**
     * Create separator
     */
    public static function separator()
    {
        $class = get_called_class();
        return new $class('', '', '', false, true);
    }
}
