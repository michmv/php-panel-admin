<?php

namespace MMV\PA\Types;

use MMV\PA\Types\MenuItemTop;

class MenuItemLeft extends MenuItemTop
{
    public $child = [];

    /**
     * @param string $title
     * @param string|array $link
     * @param string|array $resources
     * @param bool $newTab
     * @param bool $separator
     * @param array $child
     */
    public function __construct(string $title, $link='', $resources='', bool $newTab=false, bool $separator=false, array $child=[])
    {
        $this->title = $title;
        $this->link = $link;
        $this->resources = $resources;
        $this->newTab = $newTab;
        $this->separator = $separator;
        $this->child = $child;
    }

    /**
     * @param string $title
     * @param string $resources
     * @param array $child
     * @return self
     */
    public static function list($title, $resources='', $child=[])
    {
        $class = get_called_class();
        return new $class($title, '', $resources, false, false, $child);
    }
}