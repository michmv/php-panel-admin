<?php

namespace MMV\PA;

use Illuminate\Foundation\Application;
use MMV\Auth\Low\Auth as BaseAuth;
use MMV\Auth\Low\Validator;
use MMV\PA\Auth\Security;
use MMV\PA\Auth\Storage;
use MMV\PA\Auth\Environment;
use MMV\PA\Session;
use MMV\PA\Access;
use MMV\Auth\Low\User;

class Auth extends BaseAuth
{
    protected Application $app;

    public function __construct(Application $app)
    {
        $storage = new Storage($app->make('db'));
        $validator = new Validator($storage);
        $security = $app->make(Security::class);
        $session = $app->make(Session::class);
        $environment = $app->make(Environment::class);
        $access = $app->make(Access::class);

        parent::__construct($validator, $storage, $security, $session, $environment, $access);

        $this->app = $app;
    }

    public function makeUser(array $data=[]): User
    {
        $user = new User($this->validator, $this->storage);
        $user->tableName = $this->tables['users'];

        $config = $this->app->make('config');
        $user->role = $config->get('panel-admin.app.defaultRole', 0);

        foreach($user->listFiled() as $field) {
            if(array_key_exists($field, $data))
                $user->$field = $data[$field];
        }

        return $user;
    }
}
