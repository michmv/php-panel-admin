<?php

namespace MMV\PA\Backend\TableGrid;

use MMV\PA\Widgets\TableGrid\Column;

class RolesColumnDescription extends Column
{
    /**
     * @param \MMV\PA\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlCellBody($helper, $row, $index)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->t($field);
        if($this->escape) $field = $helper->e($field);

        $title = $row->title;
        if($this->translate) $title = $helper->t($title);
        if($this->escape) $title = $helper->e($title);

        return '<td class="n'.$index.'"><strong>'.$title.'</strong><br>'.$field.'</td>';
    }
}
