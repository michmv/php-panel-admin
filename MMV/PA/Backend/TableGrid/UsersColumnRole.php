<?php

namespace MMV\PA\Backend\TableGrid;

use MMV\PA\Widgets\TableGrid\Column;

class UsersColumnRole extends Column
{
    public $roles = [];

    /**
     * @param \MMV\PA\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlCellBody($helper, $row, $index)
    {
        $field = $this->field;
        $field = $row->$field;

        $value = array_key_exists($field, $this->roles) ?
            $this->roles[$field] :
            '-';

        if($this->translate) $value = $helper->t($value);
        if($this->escape) $value = $helper->e($value);

        return '<td class="n'.$index.'">'.$value.'</td>';
    }
}
