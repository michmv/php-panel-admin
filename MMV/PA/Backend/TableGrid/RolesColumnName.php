<?php

namespace MMV\PA\Backend\TableGrid;

use MMV\PA\Widgets\TableGrid\Column;

class RolesColumnName extends Column
{
    /**
     * @param \MMV\PA\Helper $helper
     * @param object $row
     * @param int $index
     * @return string
     */
    public function htmlCellBody($helper, $row, $index)
    {
        $field = $this->field;
        $field = $row->$field;
        if($this->translate) $field = $helper->t($field);
        if($this->escape) $field = $helper->e($field);

        return '<td class="n'.$index.'"><a href="'.$helper->url->route('pa.users.roles.show', [$row->id]).'">'.$field.'</a></td>';
    }
}
