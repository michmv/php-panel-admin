<?php

namespace MMV\PA\Console;

use Illuminate\Console\Command;
use MMV\PA\Auth;
use MMV\PA\Controllers\Auth as AuthController;
use Illuminate\Translation\Translator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'panel-admin:user {user} {email} {role_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user and set role for him.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(Auth $auth, Translator $trans)
    {
        $pass = 'aqswde';
        $role_id = $this->argument('role_id');

        $data = [
            'name' => $this->argument('user'),
            'email' => $this->argument('email'),
            'email_confirmed' => 1,
            'password' => $pass,
            'role' => $role_id,
        ];

        $auth->beginTransaction();

        $user = $auth->makeUser($data);
        if($user->save()) {
            // ok
            $auth->changePasswordForUser($user, $pass, $pass);
            $auth->commitTransaction();
            $this->info('The user created and set a password for him `'.$pass.'`.');
        }
        else {
            // error
            $auth->rollbackTransaction();

            foreach(AuthController::convertErrorMessage($user->getMessages(), $trans)->toArray() as $field => $value) {
                $this->error($field.':'.implode('; ', $value));
            }
        }
    }
}
