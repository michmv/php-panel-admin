<?php

return [
    'user' => 'Пользователь',

    'signin' => 'Аутентификация',
    'signup' => 'Регистрация',
    'forgotPassword' => 'Забыли пароль?',
    'changePassword' => 'Изменить пароль',

    'fieldEmail' => 'Электронная почта',
    'fieldPassword' => 'Пароль',
    'fieldNewPassword' => 'Новый пароль',
    'fieldRememberMe' => 'Запомнить меня',
    'fieldName' => 'Имя пользователя',
    'fieldConfirm' => 'Подтвердить пароль',

    'helpName' => 'Только латинские символы, цифры, тире, подчеркивание и символ точки.',

    'signinButton' => 'Войти',
    'signupButton' => 'Присоединиться',
    'forgotPasswordButton' => 'Восстановить',
    'changePasswordButton' => 'Изменить',

    'linkSignin' => 'Аутентификация',
    'linkSignup' => 'Новый пользователь',
    'linkForgotPassword' => 'Забыли пароль?',
    'linkCancel' => 'Отменить',

    'subjectConfirm' => 'Подтверждение электронной почты',
    'subjectForgotPassword' => 'Восстановить пароль',

    'messages' => [
        'successRegistration' => 'Пользователь успешно зарегистрирован, проверьте вашу почту для подтверждения регистрации.',
        'successConfirmEmail' => 'Электронная почта успешно подтверждена.',
        'createResetPassword' => 'Проверьте электронную почту, вам отправлено письмо с инструкцией как восстановить пароль.',
        'passwordHasBeenChanged' => 'Пароль был изменен.',
    ],

    'validator' => [
        'email_or_password_incorrect' => 'Неверный идентификатор пользователя и/или пароль.',
        'user_not_found' => 'Пользователь по этому идентификатору не найден.',
        'signin_was_blocked' => '',
        'password_not_verification' => 'Неверный идентификатор пользователя и/или пароль.',
        'rules_not_found' => 'Не найдено правило валидации для параметра.',
        'value_must_not_be_array' => 'Значение не должно быть массивом.',
        'value_is_required' => 'Поле обязательно для заполнения.',
        'string_max' => 'Строка должны быть не более :p1 символов.',
        'string_min' => 'Строка должны быть не менее :p1 символов.',
        'incorrect_email_format' => 'Неверный формат электронной почты.',
        'incorrect_alphanumeric' => 'Строка должна начинаться с буквы и может содержать только латинские символы, цифры, тире, подчеркивание и точку.',
        'double_separation_symbol' => 'Символы разделение не должны идти друг за другом.',
        'incorrect_bool_int' => 'Неверне значение для поля, только 0 или 1.',
        'value_not_positive_int' => 'Должно быть целым числом больше нуля.',
        'incorrect_date_format' => 'Неверный формат даты (формат: YY-MM-DD HH:MM:SS).',
        'date_invalid' => 'Указанная дата недействительна.',
        'not_unique_record' => 'Запись с таким значение уже есть.',
        'not_unique_record_exclude_this' => 'Запись с таким значение уже есть.',
        'not_confirm_password' => 'Не получилось подтвердить пароль.',
        'not_exists_in_array' => 'Необходимо выбрать один из предложенных вариантов в списка.',
        'email_not_confirm' => 'Вы не подтвердили электронную почту. Если вам не пришла инструкция для подтверждения то проверьте папку "спам" в вашем ящике. Вы можете запросить еще раз письмо для подтверждения почты через функцию восстановления пароля.',
    ],
];
