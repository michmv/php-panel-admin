<?php

return [

    /**
     * @var string
     */
    'dir' => 'uploads',

    /**
     * Allow mime type
     */
    'uploadAllow' => [
        // image
          'image/png'
        , 'image/jpeg'
        , 'image/gif'
        , 'image/svg+xml'

        // achive
        , 'application/zip'
        , 'application/x-rar-compressed'

        // doc
        , 'application/msword'
        , 'application/vnd.ms-excel'
        , 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        , 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'

        // videos
        , 'video/mp4'
        , 'video/mpeg'
        , 'video/ogg'
        , 'video/webm'
    ],

    /**
     * Example: 10M, 500K, 1G
     * 
     * @var string
     */
    'uploadMaxSize' => '5M',

];
