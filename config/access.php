<?php

use MMV\Auth\Low\Access\Resource;
use MMV\Auth\Low\Access\Role;

// new role
$user = new Role(2, 'user',
    []);
$user->title = 'panel-admin/access.title.user';
$user->description = 'panel-admin/access.description.user';

// new role
$manager = new Role(3, 'manager',
    ['backend', 'file-manager']);
$manager->title = 'panel-admin/access.title.manager';
$manager->description = 'panel-admin/access.description.manager';

// new role
$administration = new Role(1, 'administration',
    ['users.manager'],
    [$user, $manager]);
$administration->title = 'panel-admin/access.title.administration';
$administration->description = 'panel-admin/access.description.administration';

return [
    /**
     * List roles for users
     */
    'roles' => [
        $administration,
        $user,
        $manager,
    ],

    /**
     * List resources
     */
    'resources' => [
        new Resource('backend',       'panel-admin/access.resources.backend'),
        new Resource('users.manager', 'panel-admin/access.resources.managerUser', 'panel-admin/access.group.develop'),
        new Resource('file-manager',  'panel-admin/access.resources.file-manager'),
    ],
];
