<?php

return [

    /**
     * Title for html page of panel admin
     * 
     * @var string
     */
    'title' => 'panel-admin/backend.title',

    /**
     * Name route for redirect after authentication
     * 
     * @var string
     */
    'routeHome' => 'home',

    /**
     * Name route backend for redirect after authentication for administration user
     * 
     * @var string
     */
    'routeBackend' => 'pa.dashboard',

    /**
     * Id role for new user
     * 
     * @var int
     */
    'defaultRole' => 2,

    'auth' => [
        /**
         * Registration new user
         * 
         * @var bool
         */
        'registration' => true,

        /**
         * Check access fot resource after singin for redirect
         */
        'accessToBackend' => 'backend',

        /**
         * Name route
         */
        'nameRoute' => [
            'home' => 'home',
            'backend' => 'backend'
        ],
    ],

    /**
     * @see MMV\PA\Session
     */
    'session' => [
        /**
         * Name for table in database
         */
        'table' => 'sessions',

        /**
         * Name id session in cookie
         */
        'name' => 'uid',

        /**
         * How long session is valid
         */
        'duration' => 36000, // 60 * 60 * 10

        /**
         * Parameters for cookie
         */
        'cookie' => [
            'path' => '/',
            'domain' => null,
            'secure' => null
        ],

        /**
         * Crypt cookie
         */
        'crypt_cookie' => false,
    ],

    'csrf' => [
        /**
         * Name token var
         */
        'token_name' => '_token',

        /**
         * Time life fo value
         */
        'token_time_life' => 7200, // 60 * 60 * 2
    ],

    'header' => [
        'resources' => [
        ],
        'SEO' => [
            'title' => 'panel-admin/app.title',
            'keywords' => '',
            'description' => '',
        ],
    ],

];