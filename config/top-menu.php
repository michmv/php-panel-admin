<?php

use MMV\PA\Types\MenuItemTop;

return [
    MenuItemTop::item('panel-admin/backend.menu.change-password', 'pa.changePassword'),

    MenuItemTop::item('panel-admin/backend.menu.signout', 'pa.signOut'),

    MenuItemTop::separator(),

    MenuItemTop::item('panel-admin/backend.menu.signout-all', 'pa.signOutAll'),
];
