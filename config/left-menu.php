<?php

use MMV\PA\Types\MenuItemLeft;

return [
    MenuItemLeft::item('panel-admin/backend.menu.open-site', 'home', '', true),

    MenuItemLeft::list('panel-admin/backend.menu.user-manager', ['users.manager'], [

        MenuItemLeft::item('panel-admin/backend.roles.title', 'pa.users.roles'),
        MenuItemLeft::item('panel-admin/backend.users.title', 'pa.users.list'),

    ]),
];
