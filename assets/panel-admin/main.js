/**
 * @version 1.0.0
 */
///<reference path="./node_modules/@types/jquery/index.d.ts" />
var PanelAdmin = /** @class */ (function () {
    function PanelAdmin() {
        this.mod = 0;
        this.widthMod1 = 1080;
        this.minWidth = 480;
        this.topMenuStatus = false;
        this.leftMenuStatus = false;
        this.leftMenuOffset = 15;
        this.listInitFunctions = [];
        this.showLoading = false;
    }
    PanelAdmin.prototype.init = function (it) {
        var _this = this;
        this.it = it;
        this.panelAdminLeft = $('#panel-admin-left', this.it);
        this.panelAdminRight = $('#panel-admin-right', this.it);
        this.panelAdminTop = $('#panel-admin-top', this.it);
        this.resize();
        this.it.css('visibility', 'visible');
        // show top menu
        $('.pat-menu-title', this.panelAdminTop)
            .on('click', (function () { _this.topMenu(); return false; }).bind(this));
        // hide all menu
        this.it.on('click', (function () { _this.hideAll(); }).bind(this));
        // show left menu
        $('.pat-button', this.panelAdminTop)
            .on('click', (function () { _this.leftMenu(); return false; }).bind(this));
        // format left menu
        var menu = $('.pal-menu', this.panelAdminLeft);
        this.menuLeftSetOffset(menu, this.leftMenuOffset);
        this.menuLeftOpenSelect(menu);
        // set event for open menu
        $('.pal-sub > span.pal-text', this.panelAdminLeft).on('click', (function (even) { _this.subMenu(even.target); return false; }).bind(this));
        // execute custom function
        for (var index in this.listInitFunctions) {
            this.listInitFunctions[index](this);
        }
    };
    PanelAdmin.prototype.resize = function () {
        this.hideAll();
        var width = $(window).outerWidth();
        var height = $(window).height();
        if (width < this.minWidth)
            width = this.minWidth;
        // set mod
        if (width <= this.widthMod1)
            this.setMod(1);
        else
            this.setMod(2);
        var heightTopMenu = this.panelAdminTop.height();
        this.it.width(width).height(height);
        this.panelAdminLeft.height(height - heightTopMenu);
        this.panelAdminRight.height(height - heightTopMenu);
        var parContent = $('.par-content', this.panelAdminRight);
        parContent.css('min-height', this.contentMinHeight());
        if (this.mod == 2)
            this.panelAdminRight.width(width - this.panelAdminLeft.width());
        else
            this.panelAdminRight.width(width);
        // set max-width for top menu
        $('ul', this.panelAdminTop)
            .css({ "max-width": width * 0.8,
            "max-height": height - heightTopMenu
        });
    };
    PanelAdmin.prototype.addInitFunction = function (func) {
        this.listInitFunctions.push(func);
    };
    PanelAdmin.prototype.contentMinHeight = function () {
        var parContent = $('#par-content', this.panelAdminRight);
        var height = this.panelAdminLeft.height();
        return height - (parContent.outerHeight(true) - parContent.height());
    };
    /**
     * @param string type sm | lg | xl
     */
    PanelAdmin.prototype.openModal = function (content, type, footer, header, url) {
        if (type === void 0) { type = ''; }
        if (url === void 0) { url = ''; }
        // create window
        var con = $('<div>', { "class": 'modal-content' });
        var dia = $('<div>', { "class": 'modal-dialog' }).append(con);
        if (url) {
            var form = $('<form>', { action: url, method: 'POST', enctype: 'multipart/form-data' });
            con.append(form);
            con = form;
        }
        if (type)
            dia.addClass('modal-' + type);
        var mw = $('<div>', { id: 'modalWindow', "class": 'modal fade', tabindex: -1 }).append(dia);
        if (header)
            con.append($('<div>', { "class": 'modal-header' }).append(header));
        con.append($('<div>', { "class": 'modal-body' }).append(content));
        if (footer)
            con.append($('<div>', { "class": 'modal-footer' }).append(footer));
        // add content
        var box = $('#panel-admin-utility');
        box.append(mw);
        // show
        box.css({ display: 'block' });
        mw.modal();
        mw.on('hide.bs.modal', function () {
            box.empty();
            box.css({ display: 'none' });
        });
        this.modalWindow = mw;
        return mw;
    };
    /**
     * @param string type sm | lg | xl
     */
    PanelAdmin.prototype.simpleModal = function (title, content, type, footer, url) {
        if (type === void 0) { type = ''; }
        if (url === void 0) { url = ''; }
        var header = $(document.createDocumentFragment()).append([
            $('<h5 class="modal-title">' + title + '</h5>'),
            $('<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>'),
        ]);
        return this.openModal(content, type, footer, header, url);
    };
    PanelAdmin.prototype.closeModal = function () {
        if (this.modalWindow)
            this.modalWindow.modal('hide');
    };
    PanelAdmin.prototype.loading = function (on) {
        if (on) {
            this.showLoading = true;
            setTimeout(function () {
                if (this.showLoading) {
                    var l = $('<div>', { id: 'loading' });
                    l.append('<div class="cssload-container"><div class="cssload-flex-container"><span class="cssload-loading"></span></div></div>');
                    $('body').append(l);
                }
            }.bind(this), 500);
        }
        else {
            this.showLoading = false;
            $('#loading').remove();
        }
    };
    PanelAdmin.prototype.ajax = function (options) {
        options.error = function (jqXHR, textStatus, errorThrown) {
            this.loading(false);
            this.closeModal();
            this.simpleModal('Error', $('<div>', { text: jqXHR.status + ' ' + errorThrown }));
        }.bind(this);
        $.ajax(options);
    };
    PanelAdmin.prototype.hideAll = function () {
        this.topMenuHide();
        this.leftMenuHide();
    };
    PanelAdmin.prototype.leftMenu = function () {
        if (this.leftMenuStatus) {
            this.leftMenuHide();
        }
        else {
            this.leftMenuStatus = true;
            this.panelAdminLeft.addClass('pal-show');
            this.topMenuHide();
        }
    };
    PanelAdmin.prototype.leftMenuHide = function () {
        this.leftMenuStatus = false;
        this.panelAdminLeft.removeClass('pal-show');
    };
    PanelAdmin.prototype.topMenu = function () {
        if (this.topMenuStatus) {
            this.topMenuHide();
        }
        else {
            this.topMenuStatus = true;
            $('ul', this.panelAdminTop).addClass('pat-show');
            this.leftMenuHide();
        }
    };
    PanelAdmin.prototype.topMenuHide = function () {
        this.topMenuStatus = false;
        $('ul', this.panelAdminTop).removeClass('pat-show');
    };
    PanelAdmin.prototype.setMod = function (n) {
        this.mod = n;
        this.it.removeClass(['mod1', 'mod2']).addClass('mod' + n);
    };
    PanelAdmin.prototype.menuLeftSetOffset = function (ul, offset) {
        var _this = this;
        var li = ul.children('li');
        li.children('.pal-text').each(function (i, elem) {
            elem.style.paddingLeft = offset + "px";
            _this.menuLeftSetOffset(li.children('ul'), offset + 10);
        });
    };
    PanelAdmin.prototype.menuLeftOpenSelect = function (ul) {
        var select = $('.pal-select', ul);
        if (select.length) {
            select.parents('.pal-sub').addClass('pal-open');
        }
    };
    PanelAdmin.prototype.subMenu = function (item) {
        var li = $(item).parent();
        if (li.hasClass('pal-open'))
            li.removeClass('pal-open');
        else
            li.addClass('pal-open');
    };
    return PanelAdmin;
}());
var panelAdmin = new PanelAdmin();
jQuery(function () {
    panelAdmin.init($('#panel-admin'));
    $(window).on('resize', function () { return panelAdmin.resize(); });
});
//# sourceMappingURL=main.js.map