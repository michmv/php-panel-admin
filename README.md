# Panel admin

## Install

#### Install Laravel 8

https://laravel.com/docs/8.x/installation

#### Config Laravel

Config mysql connect

Config email

Install `ru` language (https://github.com/caouecs/Laravel-lang) if needed

#### Set in `config/app.php`

`locale` => `ru`

`url` => `...`

`asset_url` => `...`

`timezone` => `Europe/Moscow`

#### Install Panel Admin

In `composer.json`
```json
{
    "require": {
        "mmv/panel-admin": "^2.0"
    }
)
```

$ `composer update`

#### Add service provider to `config/app.php`

```php
<?php
'providers' => [
    // ...
    MMV\PA\Utility\ServiceProvider::class,
]
```

You can disable all aliases, they not need for this admin panel. And can disable the service providers from the list below, they are also not required for this panel admin.

- Illuminate\Auth\AuthServiceProvider
- Illuminate\Session\SessionServiceProvider
- App\Providers\AppServiceProvider
- App\Providers\AuthServiceProvider
- App\Providers\BroadcastServiceProvider
- App\Providers\EventServiceProvider

#### Congit middleware

Open `app/Http/Kernel.php` and make as show below.

```php
class Kernel extends HttpKernel
{
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \MMV\PA\Middleware\SessionStart::class,
            \MMV\PA\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        // ...
    ];
    protected $routeMiddleware = [];
}
```

#### Copy resources from package

$ `php artisan vendor:publish`

Select -> Provider: MMV\PA\Utility\ServiceProvider

#### Run migrate

$ `php artisan migrate`

#### Add routes in `routes/web.php`

```php
<?php
// ...
$router->prefix('panel-admin')->group(function(Illuminate\Routing\Router $router){
    $router->get('backend', '\MMV\PA\Controllers\Dashboard@index')->name('backend');
    MMV\PA\Utility\Routes::set($router);
});
```

#### Set home route

Replace home route and set name `home` for the it.

```php
<?php
$router->get('/', function (MMV\PA\Helper $helper) {
    return view('welcome', ['h' => $helper]);
})->name('home');
```

This name can change in `config/panel-admin/app.php` - `routeHome`

#### Create new user

$ `php artisan panel-admin:user <name> <email> 1

You can read help execute: `php artisan panel-admin:user --help`

#### Add in `welcome.blade.php`

```php
@if($h->auth()->isGuest())
    <h3>Welcome: Guest</h3>
    <ul><li><a href="{{ route('pa.signIn') }}">SignIn</a></li></ul>
@else
    <h3>Welcome: {{ $h->auth()->user()->name }}</h3>
    <ul>
        <li><a href="{{ route('pa.signOut') }}">SignOut</a></li>
        <li><a href="{{ route('pa.signOutAll') }}">SignOut from all devices</a></li>
        <li><a href="{{ route('pa.changePassword') }}">Change password</a></li>
        @if($h->auth()->check('backend'))
            <li><a href="{{ route('backend') }}">Backend</a></li>
        @endif
    </ul>
@endif
```
