<label for="field_{{ $it->name() }}">{!! $it->label !!}@if($it->required)<span class="text-danger">*</span>@endif</label>
@if($errors)
    <div class="text-danger">{!! implode('<br>', $errors) !!}</div>
@endif
<select id="field_{{ $it->name() }}" class="custom-select<?php if($errors): ?> is-invalid"<?php endif ?>" name="{{ $it->name() }}"<?php if($it->disabled): ?> disabled<?php endif ?><?php if($it->required): ?> required<?php endif ?>>
    @if($it->disableSelect)<option selected value="">{{ $it->disableSelect }}</option>@endif
    @foreach($it->options as $key => $val)
        <option @if((string)$key === (string)$value) selected @endif value="{{ $key }}">{{ $val }}</option>
    @endforeach
</select>
@if($it->help)<div class="form-text text-muted">{!! $it->help !!}</div>@endif
