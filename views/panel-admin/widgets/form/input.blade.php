@if($it->label)
    <label for="field_{{ $it->name() }}">{{ $it->label }}@if($it->required)<span class="text-danger">*</span>@endif</label>
@endif
@if($errors)
    <div class="text-danger">{{ implode('<br>', $errors) }}</div>
@endif
<input id="field_{{ $it->name() }}" class="form-control<?php if($errors): ?> is-invalid"<?php endif ?>" name="{{ $it->name() }}" type="{{ $it->type }}" value="{{ $value }}"<?php if($it->disabled): ?> disabled<?php endif ?><?php if($it->placeholder): ?> placeholder="{{ $it->placeholder }}"<?php endif ?><?php if($it->required): ?> required<?php endif ?>>
@if($it->help)<div class="form-text text-muted">{!! $it->help !!}</div>@endif
@if($it->autofocus)
<script>
    panelAdmin.addInitFunction(function(){
        $("#field_{{ $it->name() }}").focus();
    })
</script>
@endif
