@if($errors)
    <div class="text-danger">{{ implode('<br>', $errors) }}</div>
@endif
<div class="form-check">
    <input id="field_{{ $it->name() }}" class="form-check-input" name="{{ $it->name() }}" value="{{ $it->default }}" type="{{ $it->type }}"<?php if($value == $it->default and $value != ''): ?> checked<?php endif ?>@if($it->disabled) disabled<?php endif ?>>
    <label for="field_{{ $it->name() }}" class="form-check-label">{{ $it->label }}@if($it->required)<span class="text-danger">*</span>@endif</label>
</div>
@if($it->help)<div class="form-text text-muted">{!! $it->help !!}</div>@endif
