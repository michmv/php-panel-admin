<div class="alert alert-{{ $it->type }}" role="alert">
    @if($it->title)<h4 class="alert-heading">{{ $it->title }}</h4>@endif
    @foreach($messages as $message)
        @if($loop->last)<p class="mb-0">@else<p>@endif{{ $message }}</p>
    @endforeach
</div>
