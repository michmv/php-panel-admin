@if($titles = $it->getTitles())
    @php
        $res = [];
        foreach($titles as $title) {
            $res[] = $h->t($title);
        }
    @endphp
    <title>{{ implode(' | ', $res) }}</title>
@endif
@if($keywords = $it->getKeywords())
    <meta name="keywords" content="{{ $it->getKeywords() }}">
@endif
@if($description = $it->getDescription())
    <meta name="description" content="{{ $it->getDescription() }}">
@endif
@if($resources)
    {!! implode("\n", $resources) !!}
@endif
