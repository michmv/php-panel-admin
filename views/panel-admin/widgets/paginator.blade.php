<ul class="pagination pagination-sm">
    @if($prev->disable)
        <li class="page-item disabled">
            <span class="page-link"><</span>
        </li>
    @else
        <li class="page-item">
            <a href="{!! $prev->url !!}" class="page-link"><</a>
        </li>
    @endif

    @foreach($links as $link)
        @if($select == $link->n)
            <li class="page-item active" aria-current="page">
                <span class="page-link">{!! $link->n !!}<span class="sr-only">(current)</span></span>
            </li>
        @else
            @if($link->disable)
                <li class="page-item disabled"><span class="page-link">{!! $link->n !!}</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{!! $link->url !!}">{!! $link->n !!}</a></li>
            @endif
        @endif
    @endforeach

    @if($next->disable)
        <li class="page-item disabled">
            <span class="page-link">></span>
        </li>
    @else
        <li class="page-item">
            <a href="{!! $next->url !!}" class="page-link">></a>
        </li>
    @endif
</ul>
