<form @if($it->id)id="{{ $it->id }}"@endif @if($it->action)action="{{ $it->action }}"@endif @if($it->method)method="{{ $it->method }}"@endif @if($it->enctype)enctype="{{ $it->enctype}}"@endif>
{!! $h->csrf_field() !!}
@if($it->antispam)
    {{-- simple anti spam --}}
    <input class="field_email" name="email" value="">
    <input class="field_promise" name="promise" value="I am not a robot">
@endif
@if($errors)
    {!! $h->widget('MMV\PA\Widgets\Alert', ['type'=>'danger', 'message'=>$errors]) !!}
@endif
