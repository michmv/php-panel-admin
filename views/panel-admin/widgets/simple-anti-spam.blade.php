@section('simple-anti-spam-style')
<style>
    <?php if($it->id) echo '#'.$it->id.' '; ?>.field_email { display: none; }
    <?php if($it->id) echo '#'.$it->id.' '; ?>.field_promise { display: none; }
</style>
@endsection

@do($h->header->addResource('simple-anti-spam', $h->getSection('simple-anti-spam-style')))

<script>
    document.querySelector("<?php if($it->id) echo '#'.$it->id.' '; ?>.field_promise").setAttribute('value', 'I am human');
</script>
