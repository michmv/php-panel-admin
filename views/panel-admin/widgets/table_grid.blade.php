<div id="{{ $it->id }}">
    @if(count($filters))
        <div class="filters">
            @php $active = false; foreach($filters as $filter) $active = $active || $filter->active; @endphp
            <a href="#" class="<?php if(!$active) echo 'text-secondary'; ?>"><i class="fas fa-magic"></i> Фильтр</a>
            <form style="display: <?php echo $active ? 'block' : 'none'; ?>;" method="GET" action="{{ $u->current() }}">
                @foreach($params as $key => $val)
                    @if(!preg_match('/^f[0-9]+$/', $key)) <input type="hidden" name="{{ $key }}" value="{{ $val }}"> @endif
                @endforeach
                <div class="row">
                    @foreach($filters as $index => $filter)
                        {!! $filter->htmlCol($h, $index, $params, $it->colSmNum) !!}
                    @endforeach
                </div>
                <input type="submit" style="display:none;">
            </form>
        </div>
        <script>
            jQuery(function () {
                $('#{{ $it->id}} .filters a').on('click', function(){
                    var form = $('#{{ $it->id}} .filters form')
                    if(form.css('display') == 'none') form.css('display', 'block')
                    else form.css('display', 'none')
                    return false
                })
            })
        </script>
    @endif

    <div class="{{ $it->class }}">
        <table class="table mb-3">
            <thead class="thead-dark">
                <tr>
                    @foreach($columns as $index => $column)
                        {!! $column->htmlCellHead($h, $index, $params) !!}
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($collection as $index => $row)
                    <tr>
                        @foreach($columns as $index => $column)
                            {!! $column->htmlCellBody($h, $row, $index) !!}
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {!! $h->widget('MMV\PA\Widgets\Paginator', [
        'total' => $paginator['total'],
        'select' => $paginator['select'],
        'params' => $params,
        'url' => $u->current(),
    ]) !!}

    @foreach($resources as $resource)
        {!! $resource !!}
    @endforeach
</div>
