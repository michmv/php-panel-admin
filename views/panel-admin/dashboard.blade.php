@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.dashboard')))

@section('content')

<h1>{{ $h->t('panel-admin/backend.dashboard') }}</h1>

<button id="hello-world" type="button" class="btn btn-primary">Hello World!</button>
<script>
    panelAdmin.addInitFunction(function(pa){
        $('#hello-world').on('click', function(){
            pa.simpleModal('Message', $('<div>', {text: 'Hello World'}), '', $('<button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>'))
        })
    })
</script>

@endsection
