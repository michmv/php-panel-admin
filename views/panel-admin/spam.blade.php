<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style>
            body { font-size: 16px; background-color: black; color: #4ceb44; font-family: sans-serif; }
            pre { font-size: 16px; font-family: sans-serif; }
        </style>
    </head>
    <body>
        <pre id="console">_</pre>
        <script>
        var text = ["humanity@earth:~$ signin\n", "The is only for people.\n", "humanity@earth:~$ ", ""]
        var target = document.querySelector('#console')

        var map = [32,192,49,50,51,52,53,54,55,56,57,48,189,187,219,221,220,186,222,188,190,191]
        var lowercase = " `1234567890-=[]\\;',./"
        var uppercase = ' ~!@#$%^&*()_+{}|:"<>?'

        var out = function() {
            target.textContent = text.join('') + '_'
        }

        var deleteChar = function() {
            var l = text.length
            var str = text[l-1]
            if(str == '') return
            text[l-1] = str.substr(0, str.length-1)
        }

        var pressEnter = function() {
            addChar("\n")
            text.push("The is only for people.\n")
            text.push('humanity@earth:~$ ')
            text.push('')
        }

        var addChar = function(str) {
            var l = text.length
            text[l-1] = text[l-1] + str
        }

        var setChar = function(key, shift) {
            if(key >= 65 && key <= 90) {
                return String.fromCharCode(shift ? key : key + 32)
            }

            var index = indexOf(key)
            if(index >= 0) {
                var list = shift ? uppercase : lowercase
                return list[index]
            }

            return ''
        }

        var indexOf = function(n) {
            var l = map.length
            for(var i=0; i<l; i++) {
                if(n == map[i]) return i
            }
            return -1
        }

        document.addEventListener('keydown', function(event) {
            var key = event.keyCode
            if(key == 13) {
                pressEnter()
            }
            else if(key == 8) {
                deleteChar()
            }
            else {
                addChar(setChar(key, event.shiftKey))
            }

            out()
        })

        out()
        </script>
    </body>
</html>
