@extends('panel-admin.layouts.auth')

@do($h->header->setTitle('panel-admin/auth.changePassword'))

@section('content')

    <div class="h3">{{ $h->t('panel-admin/auth.changePassword') }}</div>

    {!! $h->widget('MMV\PA\Widgets\Alert', ['type'=>'info', 'message'=>$h->t('panel-admin/auth.user').': '.$user->name]) !!}

    {!! $h->widget('MMV\PA\Widgets\Form', ['antispam'=>true, 'errors'=>$errors]) !!}

        <div class="form-group">
            {!! $h->widget('MMV\PA\Widgets\Form\Password', [
                'name' => 'password',
                'label' => $h->t('panel-admin/auth.fieldNewPassword'),
                'autofocus' => true,
                'required' => true,
                'errors' => $errors,
                'value' => $data,
            ]) !!}
        </div>

        <div class="form-group">
            {!! $h->widget('MMV\PA\Widgets\Form\Password', [
                'name' => 'confirm',
                'label' => $h->t('panel-admin/auth.fieldConfirm'),
                'required' => true,
                'errors' => $errors,
                'value' => $data,
            ]) !!}
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <button type="submin" class="btn btn-primary">{{ $h->t('panel-admin/auth.changePasswordButton') }}</button>
                </div>
            </div>
            <div class="col-auton">
                <div class="form-group">
                    <a href="{{ $u->route('pa.signIn') }}" class="pl-0 btn btn-link">{{ $h->t('panel-admin/auth.linkCancel') }}</a>
                </div>
            </div>
        </div>

    </form>
    {!! $h->widget('MMV\PA\Widgets\SimpleAntiSpam') !!}

@endsection
