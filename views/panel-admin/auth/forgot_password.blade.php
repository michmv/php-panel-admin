@extends('panel-admin.layouts.auth')

@do($h->header->setTitle('panel-admin/auth.forgotPassword'))

@section('content')

    <div class="h3">{{ $h->t('panel-admin/auth.forgotPassword') }}</div>

    {!! $h->widget('MMV\PA\Widgets\Form', ['antispam'=>true, 'errors'=>$errors]) !!}

        <div class="form-group">
            {!! $h->widget('MMV\PA\Widgets\Form\Input', [
                'name' => 'email',
                'alias' => 'to',
                'label' => $h->t('panel-admin/auth.fieldEmail'),
                'type' => 'email',
                'required' => true,
                'autofocus' => true,
                'errors' => $errors,
                'value' => $data,
            ]) !!}
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <button type="submin" class="btn btn-primary">{{ $h->t('panel-admin/auth.forgotPasswordButton') }}</button>
                </div>
            </div>
            <div class="col-auton">
                <div class="form-group">
                    <a href="{{ $u->route('pa.signIn') }}" class="pl-0 btn btn-link">{{ $h->t('panel-admin/auth.linkSignin') }}</a>
                    @if($h->c('panel-admin.app.auth.registration')) <a href="{{ $u->route('pa.signUp') }}" class="pl-0 btn btn-link">{{ $h->t('panel-admin/auth.linkSignup') }}</a> @endif
                </div>
            </div>
        </div>

    </form>
    {!! $h->widget('MMV\PA\Widgets\SimpleAntiSpam') !!}

@endsection
