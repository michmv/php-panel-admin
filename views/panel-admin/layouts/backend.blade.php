<!doctype html>
<html lang="{{ $h->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="{{ $u->asset('bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ $u->asset('fontawesome/css/all.min.css') }}">
        <link rel="stylesheet" href="{{ $u->asset('panel-admin/main.css') }}">

        <script type="text/javascript" src="{{ $u->asset('panel-admin/jquery-3.5.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ $u->asset('bootstrap/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ $u->asset('panel-admin/main.js') }}"></script>

        {!! $h->widget('MMV\PA\Widgets\Header') !!}
    </head>
    <body>
        <div id="panel-admin">
            <div id="panel-admin-right" class="scroll">
                <div id="par-content" class="container-fluid">
                    @yield('content')
                </div>
            </div>

            <div id="panel-admin-left" class="scroll">
                {!! $h->widget('MMV\PA\Widgets\Menu\Left', ['select'=>$h->selectBackendMenuLeft]) !!}
            </div>

            <div id="panel-admin-top">
                <div class="pat-button"></div>
                @php $t = "Panel-Admin | " . $h->t($h->c('panel-admin.app.header.SEO.title')); @endphp
                <div class="pat-title"><a title="{{ $t }}" href="{{ route('backend') }}">{{ $t }}</a></div>
                <div class="pat-menu">
                    <div class="pat-menu-title" title="title">{{ $h->auth()->user()->name }}</div>
                    {!! $h->widget('MMV\PA\Widgets\Menu\Top') !!}
                </div>
            </div>

            <div id="panel-admin-utility"></div>
        </div>
    </body>
</html>
