<!doctype html>
<html lang="{{ $h->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="{{ $u->asset('bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ $u->asset('panel-admin/main.css') }}">

        <script type="text/javascript" src="{{ $u->asset('panel-admin/jquery-3.5.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ $u->asset('panel-admin/main.js') }}"></script>

        {!! $h->widget('MMV\PA\Widgets\Header') !!}
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center mt-5">
                <div class="col-sm-9 col-md-7 col-lg-6 col-xl-4">
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
