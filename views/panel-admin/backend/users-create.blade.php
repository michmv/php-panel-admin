@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.users.create')))

@do($h->selectBackendMenuLeft = 'panel-admin/backend.users.title')

@section('content')

{!! $h->widget('MMV\PA\Widgets\Breadcrumb', [
    'path' => [
        [$h->t('panel-admin/backend.users.title'), $u->route('pa.users.list')],
        [$h->t('panel-admin/backend.users.create')]
    ],
]) !!}

<h1>{{ $h->t('panel-admin/backend.users.create') }}</h1>

@include('panel-admin.backend._user', [
    'button' => 'panel-admin/backend.form.create',
    'roles' => $roles,
    'errors' => $errors ?? [],
])

@endsection