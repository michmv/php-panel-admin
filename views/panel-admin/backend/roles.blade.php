@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.roles.title')))

@do($h->selectBackendMenuLeft = 'panel-admin/backend.roles.title')

@section('content')

<h1>{{ $h->t('panel-admin/backend.roles.title') }}</h1>

<p>
    {{ $h->t('panel-admin/backend.roles.default') }}: <strong>{{ $roleDefault }}</strong>

    {!! $h->widget('MMV\PA\Widgets\TableGrid', [
        'strategy' => new MMV\PA\Widgets\TableGrid\Strategy\Array_,
        'collection' => $roles,
        'columns' => [
            ['field'=>'id', 'title'=>'#'],
            ['field'=>'name', 'class'=>'MMV\PA\Backend\TableGrid\RolesColumnName', 'title'=>$h->t('panel-admin/backend.roles.colName')],
            ['field'=>'description', 'class'=>'MMV\PA\Backend\TableGrid\RolesColumnDescription', 'title'=>$h->t('panel-admin/backend.roles.colDescription'), 'translate'=>true],
        ],
    ]) !!}
</p>

@endsection
