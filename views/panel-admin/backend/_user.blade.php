{!! $h->widget('MMV\PA\Widgets\Form') !!}

    <div class="form-group">
        {!! $h->widget('MMV\PA\Widgets\Form\Input', [
            'name' => 'name',
            'label' => $h->t('panel-admin/auth.fieldName'),
            'help' => $h->t('panel-admin/auth.helpName'),
            'autofocus' => true,
            'required' => true,
            'value' => $user->name ?? '',
            'errors' => $errors,
        ]) !!}
    </div>

    <div class="form-group">
        {!! $h->widget('MMV\PA\Widgets\Form\Input', [
            'name' => 'email',
            'type' => 'email',
            'label' => $h->t('panel-admin/auth.fieldEmail'),
            'required' => true,
            'value' => $user->email ?? '',
            'errors' => $errors,
        ]) !!}
    </div>

    <div class="form-group">
        {!! $h->widget('MMV\PA\Widgets\Form\Checkbox', [
            'name' => 'email_confirmed',
            'label' => $h->t('panel-admin/auth.messages.successConfirmEmail'),
            'value' => $user->email_confirmed ?? '',
            'default' => 1,
            'errors' => $errors,
        ]) !!}
    </div>

    <div class="form-group">
        {!! $h->widget('MMV\PA\Widgets\Form\Select', [
            'name' => 'role',
            'label' => __('panel-admin/backend.roles.role'),
            'required' => true,
            'disableSelect' => '-',
            'value' => $user->role ?? '',
            'options' => $roles,
            'errors' => $errors,
        ]) !!}
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">{{ $h->t($button) }}</button>
    </div>

</form>
