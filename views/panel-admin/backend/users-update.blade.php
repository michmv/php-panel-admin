@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.users.update')))

@do($h->selectBackendMenuLeft = 'panel-admin/backend.users.title')

@section('content')

{!! $h->widget('MMV\PA\Widgets\Breadcrumb', [
    'path' => [
        [$h->t('panel-admin/backend.users.title'), $u->route('pa.users.list')],
        [$h->t('panel-admin/backend.users.update')]
    ],
]) !!}

<h1>{{ $h->t('panel-admin/backend.users.update') }}</h1>

@if($session->has('message'))
    {!! $h->widget('MMV\PA\Widgets\Alert', ['type'=>'success', 'message'=>$h->t($session->get('message'))]) !!}
@endif

@include('panel-admin.backend._user', [
    'button' => 'panel-admin/backend.form.change',
    'user' => $user,
    'roles' => $roles,
    'errors' => $errors ?? [],
])

@endsection
