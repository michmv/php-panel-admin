@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.users.title')))

@do($h->selectBackendMenuLeft = 'panel-admin/backend.users.title')

@section('content')

<h1>{{ $h->t('panel-admin/backend.users.title') }}</h1>

<p><a class="btn btn-primary" href="{{ $u->route('pa.users.create') }}">{{ $h->t('panel-admin/backend.users.create') }}</a></p>

{!! $h->widget('MMV\PA\Widgets\TableGrid', [
    'strategy' => new MMV\PA\Widgets\TableGrid\Strategy\QueryBuilder,
    'collection' => $collection,
    'limit' => 20,
    'colSmNum' => 6,
    'filters' => [
        ['field'=>'id', 'class'=>'MMV\PA\Widgets\TableGrid\QueryBuilder\FilterEqual'],
        'email',
        ['field'=>'name', 'title'=>$h->t('panel-admin/backend.users.colName')],
        ['field'=>'role', 'class'=>'MMV\PA\Widgets\TableGrid\QueryBuilder\FilterSelect', 'title'=>$h->t('panel-admin/backend.users.colRole'), 'options'=>$roles],
    ],
    'columns' => [
        [   'title'=>'-',
            'class'=>'MMV\PA\Widgets\TableGrid\ColumnControl',
            'buttons'=>[
                ['icon'=>'fa-edit', 'title'=>$h->t('panel-admin/backend.edit'), 'url'=>function($h, $row) { return $h->url->route('pa.users.update', [$row->id]); }],
                ['icon'=>'fa-trash-alt', 'title'=>$h->t('panel-admin/backend.delete'), 'id'=>'btDel'],
            ],
        ],
        ['field'=>'email', 'sort'=>true],
        ['field'=>'name', 'title'=>$h->t('panel-admin/backend.users.colName'), 'sort'=>true],
        ['field'=>'role', 'class'=>'MMV\PA\Backend\TableGrid\UsersColumnRole', 'title'=>$h->t('panel-admin/backend.users.colRole'), 'roles'=>$roles, 'translate'=>true],
    ],
]) !!}

<script>
    panelAdmin.addInitFunction(function(pa){
        $('#table-grid .btDel').on('click', function(){
            var tr = $(this).parents('tr')
            var email = $('.n1', tr).text();
            var name = $('.n2', tr).text();

            var id = $(this).attr('data-id')
            var url = '{{ $u->route('pa.users.delete', [0]) }}'

            var content = $('<div class="h3 text-break">'+name+'</div><p><strong class="text-break">'+email+'</strong></p><div>{{ $h->t('panel-admin/backend.users.warning') }}</div>')
            var footer = $('<button>', {class: 'btn btn-primary', text: '{{ $h->t('panel-admin/backend.form.delete') }}'})

            footer.on('click', function(){
                pa.ajax({
                    method: 'POST',
                    data: { {{ $h->csrf_name() }}: '{{ $h->csrf_token() }}' },
                    url: url.replace('0', id),
                    success: function() {
                        pa.closeModal()
                        pa.loading(false)
                        window.location.href = window.location.href
                    }
                })
                return false
            })
            footer = [
                $('<button type="button" class="btn btn-secondary" data-dismiss="modal">{{ $h->t('panel-admin/backend.form.cancel') }}</button>'),
                footer
            ]

            pa.simpleModal('{{ $h->t('panel-admin/backend.users.question') }}', content, 'lg', footer, '')

            return false
        })
    })
</script>

@endsection
