@extends('panel-admin.layouts.backend')

@do($h->header->setTitle($h->t('panel-admin/backend.roles.role')))

@do($h->selectBackendMenuLeft = 'panel-admin/backend.roles.title')

@section('content')

@php $title = $h->t('panel-admin/backend.roles.role') . ': ' . $role->name @endphp

{!! $h->widget('MMV\PA\Widgets\Breadcrumb', [
    'path' => [
        [$h->t('panel-admin/backend.roles.title'), $u->route('pa.users.roles')],
        [$title]
    ],
]) !!}

<h1>{{ $title }}</h1>

@php
    $res = [];
    if($role->title) $res[] = '<strong>'.$h->e($h->t($role->title)).'</strong>';
    if($role->description) $res[] = $h->e($h->t($role->description));
    if($res) echo '<p>'.implode('<br>', $res).'</p>';
@endphp

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>{{ $h->t('panel-admin/backend.roles.resources') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($resources as $groupName => $groupResources)
            @if($groupName != '-')
                <tr>
                    <td colspan="2"><div class="h5">{{ $h->t($groupName) }}</div></td>
                </tr>
            @endif
            @foreach($groupResources as $resource)
                <tr>
                    <td>
                        <div class="form-check">
                            <input @if($access->checkResource($role->id, $resource->name)) checked @endif onclick="return false;" class="form-check-input" type="checkbox">
                        </div>
                    </td>
                    <td>
                        <strong>{{ $resource->name }}</strong><br />
                        {{ $h->t($resource->description) }}
                    </td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>

@endsection
